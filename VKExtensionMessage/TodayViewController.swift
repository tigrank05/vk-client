//
//  TodayViewController.swift
//  VKExtensionMessage
//
//  Created by Тигран on 28.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
	
	var unreadDialogs = 0
        
	@IBOutlet weak var unreadDialogsLabel: UILabel!
	@IBOutlet weak var friendRequestsLabel: UILabel!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		LongPollQueryServices.shared.getUpdates(completion: { [weak self] updates, error in
			guard let strongSelf = self else { return }
			if let updateModel = updates {
				updateModel.forEach {
					guard let code = $0.update.first else { return }
					switch code {
					case 4:
						strongSelf.unreadDialogs += 1
					case 6:
						strongSelf.unreadDialogs -= 1
					default:
						break
					}
				}
			}
			if !error.isEmpty {
				print(error)
			}
			strongSelf.unreadDialogsLabel.text = "\(String(describing: strongSelf.unreadDialogs.description))"
		})
		
		FriendsQueryServices.shared.getRequests { [weak self] requests, error in
			guard let strongSelf = self else { return }
			if let requests = requests {
				let requestCount = requests.count
				strongSelf.friendRequestsLabel.text = "\(requestCount.description)"
			} else if !error.isEmpty {
				print(error)
			}
		}
    }
}
