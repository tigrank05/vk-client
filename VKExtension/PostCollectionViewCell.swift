//
//  PostCollectionViewCell.swift
//  VKExtension
//
//  Created by Тигран on 28.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class PostCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var image: UIImageView! {
		didSet {
			image.layer.cornerRadius = image.bounds.width / 4
			image.clipsToBounds = true
		}
	}
}
