//
//  TodayCollectionVC.swift
//  VKExtension
//
//  Created by Тигран on 28.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayCollectionVC: UIViewController, NCWidgetProviding {
        
	// MARK: - Variables
	private let url = URL(string: "vk6351442://")!
	private let newsFeedService = NewsFeedQueryServices.shared
	private var photos = [String]()
	private let queue = OperationQueueService().queue
	
	// MARK: - Outlets
	@IBOutlet weak private var collectionView: UICollectionView!
	@IBOutlet weak private var coverView: UIView!
	@IBOutlet weak private var loginButton: UIButton!
	@IBOutlet weak private var activityIndicator: UIActivityIndicatorView!
	
	// MARK: - VCLifeCycles
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if let token = UserDefaults(suiteName: "group.vkclient.test")?.string(forKey: "AccessToken") {
			newsFeedService.token = token
		}
		
		loginButton.layer.cornerRadius = 15
		loginButton.clipsToBounds = true
		
		collectionView.delegate = self
		collectionView.dataSource = self
		
		collectionView.isHidden = true
		loginButton.isHidden = true
		
		extensionContext?.widgetLargestAvailableDisplayMode = .expanded
		
		getNewsFeed()
	}
	
	// MARK: - NCWidgetProviding
	func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
		if activeDisplayMode == .compact {
			preferredContentSize = CGSize(width: maxSize.width, height: 110)
		} else {
			preferredContentSize = CGSize(width: maxSize.width, height: collectionView.contentSize.height)
		}
	}
	
	// MARK: - Functions
	private func getNewsFeed() {
		activityIndicator.startAnimating()
		let postsInOneLine = view.frame.width / 54
		let multiplyer = (20 / postsInOneLine).rounded(.up)
		let postAmount = Int((postsInOneLine * multiplyer).rounded(.up))
		let finalPostAmount = postAmount + postAmount % 6
		self.newsFeedService.getNewsFeedFromVK(nextFrom: nil, count: 40) { [weak self] (newsResults, _, _, error) in
			guard let strongSelf = self else { return }
			strongSelf.photos.removeAll()
			strongSelf.activityIndicator.stopAnimating()
			if let news = newsResults {
				if news.count == 0 {
					strongSelf.activityIndicator.isHidden = true
					strongSelf.loginButton.isHidden = false
				} else {
					for post in news {
						if post.attachments!.count > 0 {
							if strongSelf.photos.count < finalPostAmount {
								if let photo = post.attachments?[0].photo?.bigImage {
									strongSelf.photos.append(photo)
								}
							}
						}
					}
					strongSelf.coverView.isHidden = true
					strongSelf.collectionView.isHidden = false
				}
			}
			if !error.isEmpty {
				print(error)
			}
			DispatchQueue.main.async {
				strongSelf.collectionView.reloadData()
			}
		}
	}
	
	// MARK: - Actions
	@IBAction func loginButtonPressed(_ sender: UIButton) {
		extensionContext?.open(url, completionHandler: nil)
	}
	
	// MARK: - Navigation
}

// MARK: - Extensions
extension TodayCollectionVC: UICollectionViewDelegate, UICollectionViewDataSource {
	
	// MARK: - UICollectionViewDataSource
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return photos.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postCollectionCell", for: indexPath) as! PostCollectionViewCell
		let url = photos[indexPath.item]
		let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .oneMonth)
		let setImageOp = SetCollectionImageOp(cell: cell, indexPath: indexPath, collectionView: collectionView)
		setImageOp.addDependency(getImageOp)
		queue.addOperation(getImageOp)
		OperationQueue.main.addOperation(setImageOp)
		return cell
	}
	
	// MARK: - UICollectionViewDelegate
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		extensionContext?.open(url, completionHandler: nil)
	}
}

