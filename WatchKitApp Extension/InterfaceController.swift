//
//  InterfaceController.swift
//  WatchKitApp Extension
//
//  Created by Тигран on 01.06.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController {
	
	var session: WCSession?
	var userDefaults = UserDefaults.standard
	
	@IBOutlet weak private var tableView: WKInterfaceTable!
	
    override func willActivate() {
        super.willActivate()
		
		if WCSession.isSupported() {
			session = WCSession.default
			session?.delegate = self
			session?.activate()
		}
    }
	
	private func update() {
	}

}

extension InterfaceController: WCSessionDelegate {
	
	func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
		if activationState == .activated {
			update()
			session.sendMessage(["request" : "newsFeed"], replyHandler: { reply in
				print(reply)
			}) { error in
				print(error.localizedDescription)
			}
		} else {
			print("Невозможно получить данные")
		}
	}
}
