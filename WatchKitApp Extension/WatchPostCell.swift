//
//  WatchPostCell.swift
//  WatchKitApp Extension
//
//  Created by Тигран on 06.06.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import WatchKit

class WatchPostCell: NSObject {
	
	
	@IBOutlet weak private var newsBodyLabel: WKInterfaceLabel!
	@IBOutlet weak private var newsImageView: WKInterfaceImage!

}
