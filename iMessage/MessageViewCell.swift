//
//  MessageViewCell.swift
//  iMessage
//
//  Created by Тигран on 31.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class MessageViewCell: UITableViewCell {
	
	// MARK: - Variables
	var index: IndexPath?
	private var dateService = DateService.shared
	
	// MARK: - Outlets
	@IBOutlet weak var newsBody: UILabel!
	@IBOutlet weak private var dateOfCreation: UILabel!
	@IBOutlet weak private var postersName: UILabel!
	@IBOutlet weak var newsImage: UIImageView!
	@IBOutlet weak private var postersAvatar: UIImageView!
	@IBOutlet weak private var sectionSeparator: UIView!

	// MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
		
		if index?.row == 0 {
			sectionSeparator.isHidden = true
		}
		
		newsBody.numberOfLines = 10
		postersAvatar.layer.cornerRadius = 25
		postersAvatar.clipsToBounds = true
    }
	
	func setAvatar(_ image: UIImage?) {
		postersAvatar.image = image
	}
	
	func setName(_ name: String) {
		postersName.text = name
	}
	
	func setImage(_ image: UIImage?) {
		newsImage.image = image
	}
	
	func setNewsBody(_ text: String?) {
		newsBody.text = text
	}
	
	func setDateOfCreation(_ date: Date) {
		dateOfCreation.text = dateService.dateFormater.string(for: date)
	}
}
