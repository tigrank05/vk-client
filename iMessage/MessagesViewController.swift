//
//  MessagesViewController.swift
//  iMessage
//
//  Created by Тигран on 31.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit
import Messages

class MessagesViewController: MSMessagesAppViewController {
	
	// MARK: - Variables
	private let newsFeedService = NewsFeedQueryServices.shared
	private var newsFeedModel = NewsFeedModel()
	private let queue = OperationQueueService().queue
	
	// MARK: - Outlets
	@IBOutlet weak private var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
		
		if let token = UserDefaults(suiteName: "group.vkclient.test")?.string(forKey: "AccessToken") {
			newsFeedService.token = token
		}
		
		tableView.delegate = self
		tableView.dataSource = self
		
		tableView.separatorStyle = .none
		
		getNewsFeed()
    }
	
	// MARK: - Functions
	private func getNewsFeed() {
		self.newsFeedService.getNewsFeedFromVK(nextFrom: nil, count: 30) { [weak self] (newsResults, friendsResults, groupsResults, error) in
			guard let strongSelf = self else { return }
			if let news = newsResults, let friends = friendsResults, let groups = groupsResults {
				strongSelf.newsFeedModel.clearAll()
				strongSelf.newsFeedModel.addItems(news, friends, groups)
			}
			if !error.isEmpty {
				print(error)
			}
			DispatchQueue.main.async {
				strongSelf.tableView.reloadData()
			}
		}
	}
}

extension MessagesViewController: UITableViewDataSource, UITableViewDelegate {
	
	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return newsFeedModel.newsArray.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "MessageViewCell", for: indexPath) as! MessageViewCell
		let newsFeed = newsFeedModel.newsArray[indexPath.row]
		cell.index = indexPath
		cell.setDateOfCreation(Date(timeIntervalSince1970: newsFeed.date))
		cell.setNewsBody(newsFeed.text)
		setProfile(newsFeed, cell, indexPath, tableView)
		setAttachment(newsFeed, cell, indexPath, tableView)
		return cell
	}
	
	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let layout = MSMessageTemplateLayout()
		layout.caption = (tableView.cellForRow(at: indexPath) as! MessageViewCell).newsBody.text
		layout.image = (tableView.cellForRow(at: indexPath) as! MessageViewCell).newsImage.image
		let message = MSMessage()
		message.layout = layout
		activeConversation?.insert(message, completionHandler: nil)
	}
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		let momentForLoadingMore = newsFeedModel.newsArray.count - 6
		if indexPath.row == momentForLoadingMore {
			guard let next = newsFeedModel.newsArray.last?.nextFrom else { return }
			newsFeedService.getNewsFeedFromVK(nextFrom: next, count: 15) { [weak self] (newsResults, friendsResults, groupsResults, error) in
				guard let strongSelf = self else { return }
				if let news = newsResults, let friends = friendsResults, let groups = groupsResults {
					strongSelf.newsFeedModel.addItems(news, friends, groups)
				}
				if !error.isEmpty {
					print(error)
				}
				DispatchQueue.main.async {
					strongSelf.tableView.reloadData()
				}
			}
		}
	}
	
	// MARK: - TableView Functions
	private func setProfile(_ newsFeed: NewsFeed, _ cell: MessageViewCell, _ indexPath: IndexPath, _ tableView: UITableView) {
		if newsFeed.sourceId > 0 {
			let id = newsFeed.sourceId
			let friend = newsFeedModel.getFriend(with: id)
			let getImageOp = GetCacheImageOp(url: friend.smallImage, cacheLifeTime: .oneMonth)
			let setImageOp = SetImageToProfile(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
			cell.setName(friend.getFullName())
		} else {
			let id = -1 * newsFeed.sourceId
			let group = newsFeedModel.getGroup(with: id)
			let getImageOp = GetCacheImageOp(url: group.smallImage, cacheLifeTime: .oneMonth)
			let setImageOp = SetImageToProfile(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
			cell.setName(group.name)
		}
	}
	
	private func setAttachment(_ newsFeed: NewsFeed, _ cell: MessageViewCell, _ indexPath: IndexPath, _ tableView: UITableView) {
		guard let attachments = newsFeed.attachments else { return }
		guard attachments.count > 0 else { return }
		switch attachments[0].type {
		case "photo":
			guard let url = attachments[0].photo?.bigImage else { return }
			let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .twoHours)
			let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
		case "video":
			guard let url = attachments[0].video?.bigImage else { return }
			let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .twoHours)
			let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
		default:
			break
		}
	}
}
