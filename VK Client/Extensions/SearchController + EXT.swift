//
//  SearchController + EXT.swift
//  VK Client
//
//  Created by Тигран on 06.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

extension UISearchController {
	func setupSearchController() {
		self.searchBar.placeholder = "Поиск"
		self.searchBar.barTintColor = UIColor.white
		self.searchBar.tintColor = UIColor.white
		self.dimsBackgroundDuringPresentation = false
		self.navigationItem.hidesSearchBarWhenScrolling = true
	}
}
