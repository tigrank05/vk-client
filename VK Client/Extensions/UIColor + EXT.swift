//
//  UIColor + EXT.swift
//  VK Client
//
//  Created by Тигран on 04.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

extension UIColor {
	convenience init(red: Int, green: Int, blue: Int, alpha: Double) {
		let redValue = CGFloat(red) / 255.0
		let greenValue = CGFloat(green) / 255.0
		let blueValue = CGFloat(blue) / 255.0
		let alphaValue = CGFloat(alpha)
		self.init(red: redValue, green: greenValue, blue: blueValue, alpha: alphaValue)
	}
}
