//
//  UInt64 + EXT.swift
//  VK Client
//
//  Created by Тигран on 14.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

extension UInt64 {
	func megabytes() -> UInt64 {
		return self * 1024 * 1024
	}
}
