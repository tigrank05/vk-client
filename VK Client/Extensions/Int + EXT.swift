//
//  Int + EXT.swift
//  VK Client
//
//  Created by Тигран on 01.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

extension Int {
	var membersString: String {
		let numberFormatter = NumberFormatter()
		numberFormatter.maximumFractionDigits = 1
		numberFormatter.minimumFractionDigits = 1
		numberFormatter.numberStyle = .decimal
		switch self {
		case 1000...999999:
			guard let number = numberFormatter.string(from: NSNumber(value: Double(self)/1000)) else { return String(self) }
			return "\(String(describing: number)) K"
		case 1000000...999999999:
			guard let number = numberFormatter.string(from: NSNumber(value: Double(self)/1000000)) else { return String(self) }
			return "\(String(describing: number)) M"
		case 1000000000...:
			guard let number = numberFormatter.string(from: NSNumber(value: Double(self)/1000000000)) else { return String(self) }
			return "\(number) Bl"
		default:
			return "\(self)"
		}
	}
}
