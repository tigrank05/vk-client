//
//  PostCellHeightDelegateProtocol.swift
//  VK Client
//
//  Created by Тигран on 10.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

protocol HeightDelegate: class {
	func setHeight(_ height: CGFloat, _ index: IndexPath)
}

protocol DetailHeightDelegate: class {
	func setHeight(_ height: CGFloat)
}

