//
//  AppDelegate.swift
//  VK Client
//
//  Created by Тигран on 16.02.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import RealmSwift
import VK_ios_sdk
import Firebase
import UserNotifications
import WatchConnectivity

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WCSessionDelegate {
	
    var window: UIWindow?
	var wcSession: WCSession?
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
		window?.backgroundColor = .white
		application.statusBarStyle = .lightContent
		var config = Realm.Configuration()
		config.deleteRealmIfMigrationNeeded = true
		Realm.Configuration.defaultConfiguration = config
		FirebaseApp.configure()
		GADMobileAds.configure(withApplicationID: "ca-app-pub-3940256099942544/2934735716")
		application.applicationIconBadgeNumber = 0
		
		UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .alert, .sound]) { (granted, error) in
			if granted == true {
				print("Доступ разрешен")
			} else {
				print("Доступ запрещен")
			}
			if let error = error {
				print(error.localizedDescription)
			}
		}
		application.registerForRemoteNotifications()
		
		if WCSession.isSupported() {
			wcSession = WCSession.default
			wcSession?.delegate = self
			wcSession?.activate()
		}
		
		UIApplication.shared.registerForRemoteNotifications()
		
		return true
	}
	
	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		print(deviceToken)
	}
	
	func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
		print(error)
	}
	
	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
		VKSdk.processOpen(url, fromApplication: (options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String))
		return true
	}
	
	func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
		let getUpdatesGroup = DispatchGroup()
		var completionStatus: UIBackgroundFetchResult = .noData
		var timer: DispatchSourceTimer? = nil
		var lastUpdate: Date? {
			get {
				return UserDefaults.standard.object(forKey: "Last Update") as? Date
			}
			set {
				UserDefaults.standard.setValue(Date(), forKey: "Last Update")
			}
		}
		
		print("Вызов обновления даных в фоновом режиме")
		if lastUpdate != nil, abs(lastUpdate!.timeIntervalSinceNow) < 30 {
			print("Фоновое обновление не требуется")
			completionHandler(completionStatus)
			return
		}
		
		getUpdatesGroup.enter()
		FriendsQueryServices.shared.getRequests { requests, error in
			if let requests = requests {
				let requestCount = requests.count
				print("Заявки в друзья загружены в фоне")
				completionStatus = .newData
				DispatchQueue.main.async {
					var badgeNumber = application.applicationIconBadgeNumber
					badgeNumber += requestCount
					application.applicationIconBadgeNumber = badgeNumber
					RealmServices.saveRequestsToRealm(requests)
					getUpdatesGroup.leave()
				}
			} else if !error.isEmpty {
				print("Ошибка при загрузке заявок в друзья")
				print(error)
				completionStatus = .failed
				getUpdatesGroup.leave()
			}
		}
		
		getUpdatesGroup.enter()
		LongPollQueryServices.shared.getUpdates(completion: { updates, error in
			if let updateModel = updates {
				print("Новые сообщения загружены в фоне")
				updateModel.forEach { $0.performUpdates() }
				completionStatus = .newData
				getUpdatesGroup.leave()
			}
			if !error.isEmpty {
				print("Ошибка при загрузке новых сообщений")
				print(error)
				completionStatus = .failed
				getUpdatesGroup.leave()
			}
		})
		
		getUpdatesGroup.notify(queue: DispatchQueue.main) {
			print("Все данные загружены")
			timer = nil
			lastUpdate = Date()
			completionHandler(completionStatus)
			return
		}
		
		timer = DispatchSource.makeTimerSource(queue: .main)
		timer?.schedule(deadline: .now() + 29, leeway: .seconds(1))
		timer?.setEventHandler(handler: {
			print("Не упели загрузить данные")
			getUpdatesGroup.suspend()
			
			completionHandler(.failed)
			return
		})
		timer?.resume()
	}
	
	func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
		print(message)
		if message["request"] as? String == "newsFeed" {
			NewsFeedQueryServices.shared.getNewsFeedFromVK(nextFrom: nil, count: 15) { (newsResults, _, _, error) in
				if let news = newsResults {
					let newsDict = news.map {
						["newsBody" : $0.text,
						 "newsPhotoImage" : $0.attachments?.first?.photo?.bigImage,
						 "newsVideoImage" : $0.attachments?.first?.video?.bigImage]
					}
					replyHandler(["news" : newsDict])
			}
			if !error.isEmpty {
				print(error)
			}
		}
		}
	}
	
	func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
		
	}
	
	
	func sessionDidBecomeInactive(_ session: WCSession) {
		
	}
	
	func sessionDidDeactivate(_ session: WCSession) {
		
	}
}

