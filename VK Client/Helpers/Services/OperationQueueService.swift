//
//  OperationQueueService.swift
//  VK Client
//
//  Created by Тигран on 13.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

struct OperationQueueService {
	let queue: OperationQueue = {
		let queue = OperationQueue()
		queue.qualityOfService = .userInteractive
		return queue
	}()
}
