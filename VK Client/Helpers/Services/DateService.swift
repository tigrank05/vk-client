//
//  DateFormatterService.swift
//  VK Client
//
//  Created by Тигран on 13.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

struct DateService {
	
	static let shared = DateService()
	
	private init() {}
	
	var dateFormater: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateStyle = .medium
		formatter.locale = Locale(identifier: "ru_RU")
		return formatter
	}()
	
	func formatLastDate(with date: Date?) -> String {
		guard let date = date else { return "" }
		let calendar = Calendar.current
		let dateComponents = calendar.dateComponents([.day, .month, .year, .hour, .minute], from: date)
		let currentDateComponents = calendar.dateComponents([.day, .month, .year, .hour, .minute], from: Date())
		if dateComponents.year == currentDateComponents.year {
			if dateComponents.day == currentDateComponents.day && dateComponents.month == currentDateComponents.month {
				dateFormater.timeStyle = .full
				dateFormater.dateFormat = "HH:mm"
				return dateFormater.string(from: date)
			} else if dateComponents.day == (currentDateComponents.day! - 1) && dateComponents.month == currentDateComponents.month {
				return "вчера"
			} else {
				dateFormater.timeStyle = .none
				dateFormater.dateFormat = "d MMM"
				return dateFormater.string(from: date)
			}
		} else {
			dateFormater.timeStyle = .none
			dateFormater.dateFormat = "d MMM yyyy"
			return dateFormater.string(from: date)
		}
	}
	
	func getAge(with bDay: String) -> String? {
		let formatter = DateFormatter()
		formatter.dateFormat = "d.MM.yyyy"
		var age = 0
		if let birthDate = formatter.date(from: bDay) {
			age = Int(birthDate.timeIntervalSinceNow + 14400.0) / -31536000
			if age == 1 || age > 20 && age % 10 == 1 {
				return "\(age) год"
			} else if age >= 2 && age <= 4 || age > 20 && age % 10 >= 2 && age % 10 <= 4 {
				return "\(age) года"
			} else {
				return "\(age) лет"
			}
		}
		return nil
	}
	
	func getPostDate(with date: Date) -> String {
		let calendar = Calendar.current
		let dateComponents = calendar.dateComponents([.day, .month, .year, .hour, .minute], from: date)
		let currentDateComponents = calendar.dateComponents([.day, .month, .year, .hour, .minute], from: Date())
		if dateComponents.day == currentDateComponents.day && dateComponents.month == currentDateComponents.month {
			dateFormater.timeStyle = .full
			dateFormater.dateFormat = "HH:mm"
			return "Сегодня, \(dateFormater.string(from: date))"
		} else if dateComponents.day == (currentDateComponents.day! + 1) && dateComponents.month == currentDateComponents.month {
			dateFormater.timeStyle = .full
			dateFormater.dateFormat = "HH:mm"
			return "Завтра, \(dateFormater.string(from: date))"
		} else if dateComponents.day == (currentDateComponents.day! + 2) && dateComponents.month == currentDateComponents.month {
			dateFormater.timeStyle = .full
			dateFormater.dateFormat = "HH:mm"
			return "Послезавтра, \(dateFormater.string(from: date))"
		} else {
			dateFormater.timeStyle = .full
			dateFormater.dateFormat = "d.MM.yyyy, HH:mm"
			return dateFormater.string(from: date)
		}
	}
	
	func getPublishDate(with date: Date) -> String {
		let calendar = Calendar.current
		let dateComponents = calendar.dateComponents([.day, .month, .year, .hour, .minute], from: date)
		let currentDateComponents = calendar.dateComponents([.day, .month, .year, .hour, .minute], from: Date())
		if dateComponents.day == currentDateComponents.day && dateComponents.month == currentDateComponents.month {
			dateFormater.timeStyle = .full
			dateFormater.dateFormat = "HH:mm"
			return "Опубликовать сегодня в \(dateFormater.string(from: date))"
		} else if dateComponents.day == (currentDateComponents.day! + 1) && dateComponents.month == currentDateComponents.month {
			dateFormater.timeStyle = .full
			dateFormater.dateFormat = "HH:mm"
			return "Опубликовать завтра в \(dateFormater.string(from: date))"
		} else if dateComponents.day == (currentDateComponents.day! + 2) && dateComponents.month == currentDateComponents.month {
			dateFormater.timeStyle = .full
			dateFormater.dateFormat = "HH:mm"
			return "Опубликовать послезавтра в \(dateFormater.string(from: date))"
		} else {
			dateFormater.timeStyle = .full
			dateFormater.dateFormat = "d MMM в HH:mm"
			return "Опубликовать \(dateFormater.string(from: date))"
		}
	}
}
