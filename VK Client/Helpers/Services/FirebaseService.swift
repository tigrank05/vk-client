//
//  FirebaseService.swift
//  VK Client
//
//  Created by Тигран on 26.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import FirebaseDatabase

struct FirebaseService {
	
	static func saveUserToDB() {
		let data = User.current.toAnyObject
		let dataBaseLink = Database.database().reference()
		dataBaseLink.child("Users").child("\(User.current.firstName) \(User.current.lastName)").setValue(data)
	}
}
