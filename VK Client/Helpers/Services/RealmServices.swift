//
//  RealmServices.swift
//  VK Client
//
//  Created by Тигран on 20.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import RealmSwift

struct RealmServices {
	
	static func loadFriends() {
		do {
			let realm = try Realm()
			let friendsRealm = realm.objects(Friend.self)
			guard friendsRealm.isEmpty else {
				User.current.friends.append(contentsOf: friendsRealm)
				return
			}
			FriendsQueryServices.shared.getFriends { _, error in
				if !error.isEmpty {
					print(error)
				}
			}
		} catch {
			print("Realm error: \(error.localizedDescription)")
		}
	}
	
	static func loadGroups() {
		do {
			let realm = try Realm()
			let groupsRealm = realm.objects(Group.self)
			guard groupsRealm.isEmpty else {
				User.current.groups.append(contentsOf: groupsRealm)
				return
			}
			GroupsQueryServices.shared.getMyGroups { _, error in
				if !error.isEmpty {
					print(error)
				}
			}
		} catch {
			print("Realm error: \(error.localizedDescription)")
		}
	}

	static func saveFriendsToRealm(_ friends: [Friend]) {
		do {
			let realm = try Realm()
			try realm.write {
				realm.add(friends, update: true)
			}
		} catch {
			print(error.localizedDescription)
		}
	}
	
	static func saveGroupsToRealm(_ groups: [Group]) {
		do {
			let realm = try Realm()
			try realm.write {
				realm.add(groups, update: true)
			}
		} catch {
			print(error.localizedDescription)
		}
	}
	
	static func saveRequestsToRealm(_ requests: [FriendRequests]) {
		do {
			let realm = try Realm()
			try realm.write {
				realm.add(requests, update: true)
			}
		} catch {
			print(error.localizedDescription)
		}
	}
	
	static func bind<T: Object>(_ tableView: UITableView?, to results: Results<T>?) -> NotificationToken? {
		return results?.observe { [weak tableView] changes in
			guard let strongTableView = tableView else { return }
			switch changes {
			case .initial(_):
				strongTableView.reloadData()
			case .update(_, _, _, _):
				strongTableView.reloadData()
			case .error(let error):
				print(error.localizedDescription)
				break
			}
		}
	}
	
	static func clearRealm() {
		let realmURL = Realm.Configuration.defaultConfiguration.fileURL!
		let realmURLs = [
			realmURL,
			realmURL.appendingPathExtension("lock"),
			realmURL.appendingPathExtension("note"),
			realmURL.appendingPathExtension("management")
		]
		for url in realmURLs {
			do {
				try FileManager.default.removeItem(at: url)
			} catch {
				print("Realm file deleting error: \(error.localizedDescription)")
			}
		}
	}
	
	static func filterFriends(with searchString: String) -> Results<Friend> {
		let friends: Results<Friend> = {
				let realm = try! Realm()
				return realm.objects(Friend.self).filter("firstName CONTAINS[c]%@ OR lastName CONTAINS[c]%@", searchString, searchString)
		}()
		return friends
	}
}
