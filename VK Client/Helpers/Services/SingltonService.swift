//
//  SingltonService.swift
//  VK Client
//
//  Created by Тигран on 26.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import VK_ios_sdk

struct SingltonService {
	
	static func setSingltons() {
		User.current.id = VKSdk.accessToken().userId
		User.current.firstName = VKSdk.accessToken().localUser.first_name
		User.current.lastName = VKSdk.accessToken().localUser.last_name
		
		NewsFeedQueryServices.shared.token = VKSdk.accessToken().accessToken
		NewsFeedQueryServices.shared.userId = VKSdk.accessToken().userId
		
		FriendsQueryServices.shared.token = VKSdk.accessToken().accessToken
		FriendsQueryServices.shared.userId = VKSdk.accessToken().userId
		
		GroupsQueryServices.shared.token = VKSdk.accessToken().accessToken
		GroupsQueryServices.shared.userId = VKSdk.accessToken().userId
		
		MessagesQueryServices.shared.token = VKSdk.accessToken().accessToken
		MessagesQueryServices.shared.userId = VKSdk.accessToken().userId
		
		PhotosQueryServices.shared.token = VKSdk.accessToken().accessToken
		PhotosQueryServices.shared.userId = VKSdk.accessToken().userId
		
		VideoQueryServices.shared.token = VKSdk.accessToken().accessToken
		VideoQueryServices.shared.userId = VKSdk.accessToken().userId
		
		UserQueryServices.shared.token = VKSdk.accessToken().accessToken
		UserQueryServices.shared.userId = VKSdk.accessToken().userId
		
		LocationQueryServices.shared.token = VKSdk.accessToken().accessToken
		LocationQueryServices.shared.userId = VKSdk.accessToken().userId
		
		LongPollQueryServices.shared.token = VKSdk.accessToken().accessToken
	}
	
}
