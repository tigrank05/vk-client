//
//  UserDefaultsServices.swift
//  VK Client
//
//  Created by Тигран on 28.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import VK_ios_sdk

struct UserDefaultsServices {
	
	static func saveToken() {
		let userDefaults = UserDefaults(suiteName: "group.vkclient.test")
		userDefaults?.set(VKSdk.accessToken().accessToken!, forKey: "AccessToken")
	}
}
