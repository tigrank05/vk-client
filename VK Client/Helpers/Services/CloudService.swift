//
//  CloudService.swift
//  VK Client
//
//  Created by Тигран on 24.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import CloudKit

class CloudService {
	
	static let shared = CloudService()
	
	private init() {}
	
	private lazy var record: CKRecord = {
		let recordId = CKRecord.ID(recordName: "CurrentUser")
		let record = CKRecord(recordType: "User", recordID: recordId)
		record.setValue(User.current.id, forKey: "id")
		record.setValue(User.current.firstName, forKey: "firstName")
		record.setValue(User.current.lastName, forKey: "lastName")
		return record
	}()
	
	private lazy var publicDatabase = CKContainer.default().publicCloudDatabase
	
	func saveToCloud() {
		publicDatabase.save(record) { [weak self] _, error in
			guard let strongSelf = self else { return }
			if let error = error {
				print(error.localizedDescription)
			} else {
				print("Запись \(strongSelf.record.recordID.recordName) усешно сохранена в облако")
			}
		}
	}
	
	func getFromCloud() {
		publicDatabase.fetch(withRecordID: record.recordID) { record, error in
			if let record = record {
				print("Запись \(record) загружена успешно.")
			}
			if let error = error {
				print(error.localizedDescription)
			}
		}
	}
}
