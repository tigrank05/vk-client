//
//  FriendsQueryServices.swift
//  VK Client
//
//  Created by Тигран on 02.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

final class FriendsQueryServices {
	
	static let shared = FriendsQueryServices()
	
    typealias FriendsQueryResults = ([Friend]?, String) -> ()
    
    var userId = ""
	var token = ""
	
    private let urlSession = URLSession.shared
    private var dataTask: URLSessionDataTask?
    private var friends = [Friend]()
    private var errorMessage = ""
	
	private init() {}
	
	func getFriends(completion: @escaping FriendsQueryResults) {
        dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/friends.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_ids", value: userId),
			URLQueryItem(name: "fields", value: "photo_50,photo_200_orig"),
			URLQueryItem(name: "order", value: "hints"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
        guard let url = urlComponents.url else { return }
        dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
            defer {strongSelf.dataTask = nil}
           
            if let error = error {
                strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                strongSelf.getFriendsFromVk(data)
                
                DispatchQueue.main.async {
					RealmServices.saveFriendsToRealm(strongSelf.friends)
					User.current.friends.append(contentsOf: strongSelf.friends)
                    completion(nil, strongSelf.errorMessage)
                }
            }
        }
        dataTask?.resume()
    }
	
	func getFriends(for friend: Friend, completion: @escaping FriendsQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/friends.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_id", value: "\(friend.id.description)"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getFriendsFromVk(data)
				completion(strongSelf.friends, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getFriendsFromVk(_ data: Data) {
        friends.removeAll()
        
        let parsedFriends = JSON(data)["response", "items"].arrayValue.map { Friend($0) }
		friends.append(contentsOf: parsedFriends)
		
    }
	
	func updateFriendsStatus(completion: @escaping FriendsQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/friends.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_ids", value: userId),
			URLQueryItem(name: "fields", value: "online"),
			URLQueryItem(name: "order", value: "hints"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getFriendsFromVk(data)
				
				DispatchQueue.main.async {
					completion(strongSelf.friends, strongSelf.errorMessage)
				}
			}
		}
		dataTask?.resume()
	}
	
	func deleteFriend(with id: Int) {
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/friends.delete"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_id", value: "\(id.description)"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		var urlRequest = URLRequest(url: url)
		urlRequest.httpMethod = "POST"
		let task = urlSession.dataTask(with: urlRequest)
		task.resume()
	}
	
	func addFriend(with id: Int, and message: String) {
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/friends.add"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_id", value: "\(id.description)"),
			URLQueryItem(name: "text", value: "\(message)"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		var urlRequest = URLRequest(url: url)
		urlRequest.httpMethod = "POST"
		let task = urlSession.dataTask(with: urlRequest)
		task.resume()
	}
	
	func getRequests(completion: @escaping ([FriendRequests]?, String) -> () ) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/friends.getRequests"
		urlComponents.queryItems = [
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				let requests = JSON(data)["response", "items"].arrayValue.map { FriendRequests($0) }
				UserDefaults.standard.setValue(Date(), forKey: "Last Update")
				completion(requests, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
}
