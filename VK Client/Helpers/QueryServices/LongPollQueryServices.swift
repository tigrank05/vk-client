//
//  LongPollQueryServices.swift
//  VK Client
//
//  Created by Тигран on 15.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

final class LongPollQueryServices {
	
	static let shared = LongPollQueryServices()
	
	typealias UpdatesQueryResults = ([Updates]?, String) -> ()
	
	var token = ""
	
	private let urlSession = URLSession.shared
	private var dataTask: URLSessionDataTask?
	private var errorMessage: String = ""
	private var updates = [Updates]()
	private var longPollAnswer: LongPollAnswer?
	
	private init() {}
	
	func connectToLongPollServer(completion: @escaping () -> () ) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/messages.getLongPollServer"
		urlComponents.queryItems = [
			URLQueryItem(name: "need_pts", value: "1"),
			URLQueryItem(name: "lp_version", value: "3"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url, completionHandler: { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				let answer = LongPollAnswer(JSON(data)["response"])
				strongSelf.longPollAnswer = answer
				completion()
			}
		})
		dataTask?.resume()
	}
	
	func getUpdates(completion: @escaping UpdatesQueryResults) {
		guard let answer = longPollAnswer else { return }
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.path = answer.server
		urlComponents.queryItems = [
			URLQueryItem(name: "act", value: "a_check"),
			URLQueryItem(name: "key", value: answer.key),
			URLQueryItem(name: "ts", value: answer.ts.description),
			URLQueryItem(name: "wait", value: "25"),
			URLQueryItem(name: "mode", value: "10"),
			URLQueryItem(name: "version", value: "2"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url, completionHandler: { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer { strongSelf.dataTask = nil }
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.updates.removeAll()
				strongSelf.longPollAnswer?.ts = JSON(data)["ts"].intValue
				let updatesArray = JSON(data)["updates"].arrayValue.map { $0.arrayValue.map { $0.intValue } }
				updatesArray.forEach {
					let tempUpdate = Updates($0)
					strongSelf.updates.append(tempUpdate)
				}
				completion(strongSelf.updates, strongSelf.errorMessage)
			}
		})
		dataTask?.resume()
	}
}
