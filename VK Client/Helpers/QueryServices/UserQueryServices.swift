//
//  UserQueryServices.swift
//  VK Client
//
//  Created by Тигран on 17.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

final class UserQueryServices {
	
	static let shared = UserQueryServices()
	
	typealias UserQueryResults = (Friend?, String) -> ()
	
	var userId = ""
	var token = ""
	
	private let urlSession = URLSession.shared
	private var dataTask: URLSessionDataTask?
	private var user: Friend?
	private var errorMessage = ""
	
	private init() {}
	
	func getUser(with id: String, completion: @escaping UserQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/users.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_ids", value: "\(id)"),
			URLQueryItem(name: "fields", value: "bdate,city,photo_50,photo_200_orig,online,contacts,site,education,universities,schools,status,last_seen,followers_count,common_count,occupation,relatives,relation,personal,connections,exports,activities,interests,about,quotes,timezone,is_friend,career,military"),
			URLQueryItem(name: "order", value: "hints"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getUserFromVk(data)
				completion(strongSelf.user, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getUserFromVk(_ data: Data) {
		user = nil
		
		let parsedUser = JSON(data)["response"].arrayValue.map { Friend(user: $0) }
		user = parsedUser[0]
		
	}
}
