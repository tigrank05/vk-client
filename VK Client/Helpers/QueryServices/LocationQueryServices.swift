//
//  LocationQueryServices.swift
//  VK Client
//
//  Created by Тигран on 14.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON
import CoreLocation

final class LocationQueryServices {
	
	static let shared = LocationQueryServices()
	
	typealias LocationsQueryResults = ([Location]?, String) -> ()
	
	var userId = ""
	var token = ""
	
	private let urlSession = URLSession.shared
	private var dataTask: URLSessionDataTask?
	private var locationsArray = [Location]()
	private var errorMessage = ""
	
	private init() {}
	
	func getLocations(for coordinates: CLLocationCoordinate2D, completion: @escaping LocationsQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/places.search"
		urlComponents.queryItems = [
			URLQueryItem(name: "latitude", value: "\(coordinates.latitude.description)"),
			URLQueryItem(name: "longitude", value: "\(coordinates.longitude.description)"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self ] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getLocationsFromVK(data)
				completion(strongSelf.locationsArray, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getLocationsFromVK(_ data: Data) {
		locationsArray.removeAll()
		let locations = JSON(data)["response", "items"].arrayValue.map { Location($0) }
		locationsArray.append(contentsOf: locations)
	}
}
