//
//  NewsFeedQueryServices.swift
//  VK Client
//
//  Created by Тигран on 27.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

final class NewsFeedQueryServices {
	
	static let shared = NewsFeedQueryServices()
	
	typealias NewsFeedQueryResults = ([NewsFeed]?, [Friend]?, [Group]?, String) -> ()
	
	var token = ""
	var userId = ""
	
	private let urlSession = URLSession.shared
	private var dataTask: URLSessionDataTask?
	private var news: [NewsFeed] = []
	private var friends: [Friend] = []
	private var groups: [Group] = []
	private var errorMessage = ""
	
	private init() {}
	
	func getNewsFeedFromVK(nextFrom: String?, count: Int, completion: @escaping NewsFeedQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/newsfeed.get"
		if let next = nextFrom {
			urlComponents.queryItems = [
				URLQueryItem(name: "filters", value: "post"),
				URLQueryItem(name: "count", value: "\(count.description)"),
				URLQueryItem(name: "return_banned", value: "0"),
				URLQueryItem(name: "start_from", value: "\(next)"),
				URLQueryItem(name: "access_token", value: token),
				URLQueryItem(name: "v", value: "5.68")]
		} else {
			urlComponents.queryItems = [
				URLQueryItem(name: "filters", value: "post"),
				URLQueryItem(name: "count", value: "\(count.description)"),
				URLQueryItem(name: "return_banned", value: "0"),
				URLQueryItem(name: "access_token", value: token),
				URLQueryItem(name: "v", value: "5.68")]
		}
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.parseNewsFeedFromData(data)
				
				DispatchQueue.main.async {
					completion(strongSelf.news, strongSelf.friends, strongSelf.groups, strongSelf.errorMessage)
				}
			}
		}
		dataTask?.resume()
	}
	
	func getWallFromVK(for id: String, completion: @escaping NewsFeedQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/wall.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "owner_id", value: "\(id)"),
			URLQueryItem(name: "filter", value: "owner"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.parseNewsFeedFromData(data)
				
				DispatchQueue.main.async {
					completion(strongSelf.news, strongSelf.friends, strongSelf.groups, strongSelf.errorMessage)
				}
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func parseNewsFeedFromData(_ data: Data) {
		news.removeAll()
		friends.removeAll()
		groups.removeAll()
		
		let parsedNews = JSON(data)["response", "items"].arrayValue.map { NewsFeed($0) }
		let nextFrom = JSON(data)["response", "next_from"].stringValue
		parsedNews.forEach { $0.nextFrom = nextFrom }

		let parsedFriends = JSON(data)["response", "profiles"].arrayValue.map { Friend($0) }
		let parsedGroups = JSON(data)["response", "groups"].arrayValue.map { Group($0) }
		
		news.append(contentsOf: parsedNews)
		friends.append(contentsOf: parsedFriends)
		groups.append(contentsOf: parsedGroups)
	}
	
	func postOnWall(with message: String, lat: Double?, long: Double?, placeId: Int, photos: [Photo]?, isPrivate: Bool, publishDate: Date?) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/wall.post"
		var queryItems = [URLQueryItem(name: "owner_id", value: "\(userId)"), URLQueryItem(name: "message", value: message)]
		if lat != nil, long != nil {
			queryItems.append(contentsOf: [URLQueryItem(name: "lat", value: lat!.description),
										   URLQueryItem(name: "long", value: long!.description),
										   URLQueryItem(name: "place_id", value: placeId.description)])
		}
		if let photos = photos {
			var finalPhotoQueryString = ""
			for photo in photos {
				let photoQueryString = "photo\(photo.ownerId.description)_\(photo.id.description),"
				finalPhotoQueryString += photoQueryString
			}
			finalPhotoQueryString.removeLast()
			let photoQueryItem = URLQueryItem(name: "attachments", value: finalPhotoQueryString)
			queryItems.append(photoQueryItem)
		}
		if isPrivate {
			let privateQueryItem = URLQueryItem(name: "friends_only", value: "1")
			queryItems.append(privateQueryItem)
		}
		if let date = publishDate {
			let dateQueryItem = URLQueryItem(name: "publish_date", value: "\(date.timeIntervalSince1970)")
			queryItems.append(dateQueryItem)
		}
		queryItems.append(contentsOf: [
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")])
		urlComponents.queryItems = queryItems
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url)
		dataTask?.resume()
	}
}
