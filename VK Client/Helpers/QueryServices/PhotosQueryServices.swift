//
//  PhotosQueryServices.swift
//  VK Client
//
//  Created by Тигран on 17.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON
import Alamofire

enum PhotosCount: String {
	case max = "1000"
	case forSmoothWork = "50"
}

final class PhotosQueryServices {
	
	static let shared = PhotosQueryServices()
	
	typealias PhotosQueryResults = ([Photo]?, String) -> ()
	
	var userId = ""
	var token = ""
	
	private let urlSession = URLSession.shared
	private var dataTask: URLSessionDataTask?
	private var photos = [Photo]()
	private var errorMessage = ""
	
	private init() {}
	
	func getWallPhotos(for friend: Friend, count: PhotosCount, completion: @escaping PhotosQueryResults) {
		dataTask?.cancel()
		
		let friendId = friend.id.description
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/photos.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_id", value: "\(friendId)"),
			URLQueryItem(name: "album_id", value: "wall"),
			URLQueryItem(name: "count", value: count.rawValue),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getPhotosFromVk(data)
				completion(strongSelf.photos, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getPhotosFromVk(_ data: Data) {
		photos.removeAll()
		
		let parsedPhotos = JSON(data)["response", "items"].arrayValue.map { Photo(personal: $0) }
		photos.append(contentsOf: parsedPhotos)
		
	}
	
	func uploadWallPhotoToServer(_ imageUrl: URL, with imageName: String, latitude: Double?, longitude: Double?, completion: @escaping ([Photo]?, String) -> ()) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/photos.getWallUploadServer"
		urlComponents.queryItems = [
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				let uploadUrl = JSON(data)["response", "upload_url"].stringValue
				strongSelf.makePostRequest(uploadUrl, imageUrl: imageUrl, imageName: imageName, latitude: latitude, longitude: longitude, postCompletion: { server, hash, photo in
					strongSelf.saveWallPhoto(photo, server: server, hash: hash, lalitude: latitude, longitude: longitude, saveCompletion: { postedPhotos in
						if let postedPhotos = postedPhotos {
							completion(postedPhotos, strongSelf.errorMessage)
						}
						else {
							completion(nil, strongSelf.errorMessage)
						}
						
					})
				
				})
			}
		}
		dataTask?.resume()
	}
	
	func makePostRequest(_ uploadUrl: String, imageUrl: URL, imageName: String, latitude: Double?, longitude: Double?, postCompletion: @escaping (Int, String, String) -> ()) {
		guard let url = URL(string: uploadUrl) else { return }
		
		Alamofire.upload(multipartFormData: { multipartFormData in
			multipartFormData.append(imageUrl, withName: "photo")
		}, to: url) { encodingResults in
			switch encodingResults {
			case .success(let upload, _, _):
				upload.responseJSON(completionHandler: { response in
					let server = JSON(response.data!)["server"].intValue
					let hash = JSON(response.data!)["hash"].stringValue
					let photo = JSON(response.data!)["photo"].stringValue
					postCompletion(server, hash, photo)
				})
			case .failure(let encodingError):
				print(encodingError)
			}
		}
	}
	
	func saveWallPhoto(_ photo: String, server: Int, hash: String, lalitude: Double?, longitude: Double?, saveCompletion: @escaping ([Photo]?) -> ()) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/photos.saveWallPhoto"
		if let lalitude = lalitude, let longitude = longitude {
			urlComponents.queryItems = [
				URLQueryItem(name: "user_id", value: userId),
				URLQueryItem(name: "photo", value: photo),
				URLQueryItem(name: "server", value: server.description),
				URLQueryItem(name: "hash", value: hash),
				URLQueryItem(name: "latitude", value: lalitude.description),
				URLQueryItem(name: "longitude", value: longitude.description),
				URLQueryItem(name: "access_token", value: token),
				URLQueryItem(name: "v", value: "5.68")]
		} else {
			urlComponents.queryItems = [
				URLQueryItem(name: "user_id", value: userId),
				URLQueryItem(name: "photo", value: photo),
				URLQueryItem(name: "server", value: server.description),
				URLQueryItem(name: "hash", value: hash),
				URLQueryItem(name: "access_token", value: token),
				URLQueryItem(name: "v", value: "5.68")]
		}
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				let postedPhoto = JSON(data)["response"].arrayValue.map { Photo(personal: $0) }
				saveCompletion(postedPhoto)
			}
		}
		dataTask?.resume()
	}
}
