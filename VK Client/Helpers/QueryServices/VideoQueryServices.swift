//
//  VideoQueryServices.swift
//  VK Client
//
//  Created by Тигран on 17.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//


import SwiftyJSON

final class VideoQueryServices {
	
	static let shared = VideoQueryServices()
	
	typealias VideosQueryResults = (Int?, String) -> ()
	
	var userId = ""
	var token = ""
	
	private let urlSession = URLSession.shared
	private var dataTask: URLSessionDataTask?
	private var videosCount: Int?
	private var errorMessage = ""
	
	private init() {}
	
	func getVideos(for friend: Friend, completion: @escaping VideosQueryResults) {
		dataTask?.cancel()
		
		let friendId = friend.id.description
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/video.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_id", value: "\(friendId)"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getVideosFromVk(data)
				completion(strongSelf.videosCount, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getVideosFromVk(_ data: Data) {
		videosCount = nil
		
		let videos = JSON(data)["response", "count"].intValue
		videosCount = videos
		
	}
}
