//
//  MessagesQueryServices.swift
//  VK Client
//
//  Created by Тигран on 23.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

final class MessagesQueryServices {
	
	static let shared = MessagesQueryServices()
	
	typealias MessagesQueryResults = ([Message]?, String) -> ()
	typealias DialogsQueryResults = ([Dialog]?, [Profile]?, [Group]?, String) -> ()
	typealias UsersQueryResults = ([Profile]?, String) -> ()
	typealias GroupsQueryResults = ([Group]?, String) -> ()
	
	var userId = ""
	var token = ""
	
	private let urlSession = URLSession.shared
	private var dataTask: URLSessionDataTask?
	private var dialogsArray = [Dialog]()
	private var usersArray = [Profile]()
	private var groupsArray = [Group]()
	private var messagesArray = [Message]()
	private var errorMessage = ""
	private var ids = [Int]()
	
	private init() {}
	
	func getDialogs(completion: @escaping DialogsQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/messages.getDialogs"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_ids", value: userId),
			URLQueryItem(name: "count", value: "30"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getDialogsFromVk(data)
				strongSelf.getUsersById(strongSelf.ids, completion: { (_, _) in
					strongSelf.getGroupsById(strongSelf.ids, completion: { (_, _) in
						DispatchQueue.main.async {
							completion(strongSelf.dialogsArray, strongSelf.usersArray, strongSelf.groupsArray, strongSelf.errorMessage)
						}
					})
				})
			}
		}
		dataTask?.resume()
	}
	
	func getDialogs(fromMessage id: Int, completion: @escaping DialogsQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/messages.getDialogs"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_ids", value: userId),
			URLQueryItem(name: "start_message_id", value: id.description),
			URLQueryItem(name: "count", value: "30"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getDialogsFromVk(data)
				strongSelf.getUsersById(strongSelf.ids, completion: { (_, _) in
					strongSelf.getGroupsById(strongSelf.ids, completion: { (_, _) in
						DispatchQueue.main.async {
							completion(strongSelf.dialogsArray, strongSelf.usersArray, strongSelf.groupsArray, strongSelf.errorMessage)
						}
					})
				})
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getDialogsFromVk(_ data: Data) {
		dialogsArray.removeAll()
		
		let dialogs = JSON(data)["response", "items"].arrayValue.map { Dialog($0) }
		dialogsArray.append(contentsOf: dialogs)
		for dialog in dialogs {
			ids.append(dialog.message.userId)
		}
	}
	
	fileprivate func getUsersById(_ ids: [Int], completion: @escaping UsersQueryResults) {
		dataTask?.cancel()
		
		var stringIds: String = ""
		for id in ids {
			if id > 0 {
				stringIds += "\(id),"
			}
		}
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/users.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_ids", value: stringIds),
			URLQueryItem(name: "fields", value: "photo_50,photo_200_orig"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getUsersFromVk(data)
				completion(strongSelf.usersArray, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getGroupsById(_ ids: [Int], completion: @escaping GroupsQueryResults) {
		dataTask?.cancel()
		
		var stringIds: String = ""
		for id in ids {
			if id < 0 {
				stringIds += "\(-1 * id),"
			}
		}
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/groups.getById"
		urlComponents.queryItems = [
			URLQueryItem(name: "group_ids", value: stringIds),
			URLQueryItem(name: "fields", value: "photo_50,photo_200"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getGroupsFromVK(data)
				completion(strongSelf.groupsArray, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getUsersFromVk(_ data: Data) {
		usersArray.removeAll()
		
		let users = JSON(data)["response"].arrayValue.map { Profile($0) }
		usersArray.append(contentsOf: users)
	}
	
	private func getGroupsFromVK(_ data: Data) {
		groupsArray.removeAll()
		
		let groups = JSON(data)["response"].arrayValue.map { Group($0) }
		groupsArray.append(contentsOf: groups)
	}
	
	func getMessages(with id: Int, from lastMessage: Int, completion: @escaping MessagesQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/messages.getHistory"
		urlComponents.queryItems = [
			URLQueryItem(name: "count", value: "200"),
			URLQueryItem(name: "user_id", value: id.description),
			URLQueryItem(name: "start_message_id", value: lastMessage.description),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getMessagesFromVk(data)
				completion(strongSelf.messagesArray, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getMessagesFromVk(_ data: Data) {
		messagesArray.removeAll()
		
		let messages = JSON(data)["response", "items"].arrayValue.map { Message($0) }
		for message in messages {
			if message.photo?.bigImage == "" {
				message.photo = nil
			}
		}
		messagesArray.append(contentsOf: messages)
	}
	
	func sendMessage(forUserWithId id: Int?, message: String) {
		dataTask?.cancel()
		
		guard let friendId = id?.description else { return }
		let stringId = String(friendId)
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/messages.send"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_id", value: stringId),
			URLQueryItem(name: "message", value: message),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url)
		dataTask?.resume()
	}
	
	func markAsRead(from messageId: Int) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/messages.markAsRead"
		urlComponents.queryItems = [
			URLQueryItem(name: "peer_id", value: userId),
			URLQueryItem(name: "start_message_id", value: messageId.description),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		let urlRequest = URLRequest(url: url)
		dataTask = urlSession.dataTask(with: urlRequest)
		dataTask?.resume()
	}
	
	// TODO: Написать метод отправки сообщения с вложением
}
