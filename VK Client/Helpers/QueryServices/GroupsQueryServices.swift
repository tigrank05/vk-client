//
//  GroupsQueryServices.swift
//  VK Client
//
//  Created by Тигран on 20.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

final class GroupsQueryServices {
	
	static let shared = GroupsQueryServices()

	typealias AllGroupsQueryResults = ([Group]?, String) -> ()
	typealias MyGroupsQueryResults = ([Group]?, String) -> ()
	
	var userId = ""
	var token = ""
	
	private let urlSession = URLSession.shared
	private var dataTask: URLSessionDataTask?
	private var groups = [Group]()
	private var errorMessage = ""
	
	private init() {}
	
	func getMyGroups(completion: @escaping MyGroupsQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/groups.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_ids", value: userId),
			URLQueryItem(name: "extended", value: "1"),
			URLQueryItem(name: "fields", value: "members_count"),
			URLQueryItem(name: "order", value: "hints"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return}
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getMyGroupsFromVk(data)
				
				DispatchQueue.main.async {
					RealmServices.saveGroupsToRealm(strongSelf.groups)
					User.current.groups.append(contentsOf: strongSelf.groups)
					completion(nil, strongSelf.errorMessage)
				}
			}
		}
		dataTask?.resume()
	}
	
	func getGroups(for friend: Friend, completion: @escaping MyGroupsQueryResults) {
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/groups.get"
		urlComponents.queryItems = [
			URLQueryItem(name: "user_id", value: "\(friend.id.description)"),
			URLQueryItem(name: "extended", value: "1"),
			URLQueryItem(name: "fields", value: "members_count"),
			URLQueryItem(name: "order", value: "hints"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.getMyGroupsFromVk(data)
				completion(strongSelf.groups, strongSelf.errorMessage)
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func getMyGroupsFromVk(_ data: Data) {
		groups.removeAll()
		
		let parsedGroups = JSON(data)["response", "items"].arrayValue.map { Group($0) }
		groups.append(contentsOf: parsedGroups)
	}
	
	func searchAllGroup(searchString: String, completion: @escaping AllGroupsQueryResults) {
		groups.removeAll()
		guard searchString != "" else {
			DispatchQueue.main.async { [weak self] in
				guard let strongSelf = self else { return }
				completion(strongSelf.groups, strongSelf.errorMessage)
			}
			return
		}
		
		dataTask?.cancel()
		
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/groups.search"
		urlComponents.queryItems = [
			URLQueryItem(name: "q", value: searchString),
			URLQueryItem(name: "fields", value: "members_count"),
			URLQueryItem(name: "sort", value: "0"),
			URLQueryItem(name: "access_token", value: token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
			guard let strongSelf = self else { return }
			defer {strongSelf.dataTask = nil}
			
			if let error = error {
				strongSelf.errorMessage += "DataTask error: \(error.localizedDescription)\n"
			} else if let data = data,
				let response = response as? HTTPURLResponse,
				response.statusCode == 200 {
				strongSelf.updateSearchResults(data)
				
				DispatchQueue.main.async {
					completion(strongSelf.groups, strongSelf.errorMessage)
				}
			}
		}
		dataTask?.resume()
	}
	
	fileprivate func updateSearchResults(_ data: Data) {
		groups.removeAll()
		
		let parsedGroups = JSON(data)["response", "items"].arrayValue.map { Group($0) }
		groups.append(contentsOf: parsedGroups)
	}

}
