//
//  SetImageToRow.swift
//  VK Client
//
//  Created by Тигран on 04.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

final class SetImageOp: Operation {
	private let indexPath: IndexPath
	private weak var tableView: UITableView?
	private let cell: UITableViewCell?
	
	init(cell: UITableViewCell, indexPath: IndexPath, tableView: UITableView) {
		self.indexPath = indexPath
		self.tableView = tableView
		self.cell = cell
	}
	
	override func main() {
		guard let tableView = tableView
			, let cell = cell
			, let getCachedImage = dependencies[0] as? GetCacheImageOp
			, let image = getCachedImage.outputImage else { return }
		
		if let newIndexPath = tableView.indexPath(for: cell), newIndexPath == indexPath {
			setImage(image, to: cell)
		}
	}
	
	private func setImage(_ image: UIImage, to cell: UITableViewCell) {
		if let cell = cell as? FriendsTableCell {
			cell.setImage(nil)
			cell.setImage(image)
		} else if let cell = cell as? GroupsCell {
			cell.setImage(nil)
			cell.setImage(image)
		} else if let cell = cell as? PostCell {
			cell.setImage(nil)
			cell.setImage(image)
		} else if let cell = cell as? DialogCell {
			cell.setImage(nil)
			cell.setImage(image)
		} else if let cell = cell as? FriendsPageHeaderTableCell {
			cell.setImage(nil)
			cell.setImage(image)
		} else if let cell = cell as? LocationCell {
			cell.setImage(nil)
			cell.setImage(image)
		} else if let cell = cell as? MessageViewCell {
			cell.setImage(nil)
			cell.setImage(image)
		}
	}
}

final class SetCollectionImageOp: Operation {
	private let indexPath: IndexPath
	private weak var collectionView: UICollectionView?
	private let cell: UICollectionViewCell?
	
	init(cell: UICollectionViewCell, indexPath: IndexPath, collectionView: UICollectionView) {
		self.indexPath = indexPath
		self.collectionView = collectionView
		self.cell = cell
	}
	
	override func main() {
		guard let collectionView = collectionView
			, let cell = cell
			, let getCachedImage = dependencies[0] as? GetCacheImageOp
			, let image = getCachedImage.outputImage else { return }
		
		if let newIndexPath = collectionView.indexPath(for: cell), newIndexPath == indexPath {
			setImage(image, to: cell)
		} else if collectionView.indexPath(for: cell) == nil {
			setImage(image, to: cell)
		}
	}
	
	private func setImage(_ image: UIImage, to cell: UICollectionViewCell) {
		if let cell = cell as? FriendPhotosCollectionViewCell {
			cell.photo.image = nil
			cell.photo.image = image
		} else if let cell = cell as? PostCollectionViewCell {
			cell.image.image = nil
			cell.image.image = image
		}
	}
}


class SetImageToProfile: Operation {
	private let indexPath: IndexPath
	private weak var tableView: UITableView?
	private var cell: UITableViewCell?
	
	init(cell: UITableViewCell, indexPath: IndexPath, tableView: UITableView) {
		self.indexPath = indexPath
		self.tableView = tableView
		self.cell = cell
	}
	
	override func main() {
		guard let tableView = tableView
			, let cell = cell
			, let getCachedImage = dependencies[0] as? GetCacheImageOp
			, let image = getCachedImage.outputImage else { return }
		
		if let newIndexPath = tableView.indexPath(for: cell), newIndexPath == indexPath {
			setImage(image, to: cell)
		} else if tableView.indexPath(for: cell) == nil {
			setImage(image, to: cell)
		}
	}
	
	private func setImage(_ image: UIImage, to cell: UITableViewCell) {
		if let cell = cell as? PostCell {
			cell.setAvatar(nil)
			cell.setAvatar(image)
		} else if let cell = cell as? MessageViewCell {
			cell.setAvatar(nil)
			cell.setAvatar(image)
		}
	}
}
