//
//  GetCacheImage.swift
//  VK Client
//
//  Created by Тигран on 03.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import AlamofireImage

final class GetCacheImageOp: Operation {
	enum CacheLifeTime: Double { case twoHours = 7200.0, oneMonth = 2592000.0 }
	
	var outputImage: UIImage?
	private let url: String
	private let identifier: String
	private let cacheLifeTime: CacheLifeTime
	private static let imageCache = AutoPurgingImageCache(memoryCapacity: UInt64(100).megabytes(), preferredMemoryUsageAfterPurge: UInt64(60).megabytes())
	
	init(url: String, cacheLifeTime: CacheLifeTime) {
		self.url = url
		self.cacheLifeTime = cacheLifeTime
		self.identifier = url.hashValue.description
	}
	
	override func main() {
		guard !isCancelled else { return }
		if getImagesFromCache() { return }
		
		guard !isCancelled else { return }
		if !downloadImage() { return }
		
		guard let image = outputImage else { return }
		
		guard !isCancelled else { return }
		saveImageToCache(image)
	}
	
	private func getImagesFromCache() -> Bool {
		guard let cachedImage = GetCacheImageOp.imageCache.image(withIdentifier: identifier) else { return false }
		self.outputImage = cachedImage
		return true
	}
	
	private func downloadImage() -> Bool {
		guard let url = URL(string: url), let data = try? Data(contentsOf: url), let image = UIImage(data: data) else { return false }
		self.outputImage = image
		return true
	}
	
	private func saveImageToCache(_ image: UIImage) {
		GetCacheImageOp.imageCache.add(image, withIdentifier: identifier)
	}
}
