//
//  GroupsTableVC.swift
//  VK Client
//
//  Created by Тигран on 21.02.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import RealmSwift

final class GroupsTableVC: UITableViewController {

    // MARK: - Variables
	private let groupsQuery = GroupsQueryServices.shared
	private let groupsModel = RealmGroupsModel()
	private let filteredGroupsModel = NativeGroupsModel()
	private var token: NotificationToken?
	private var searchController = UISearchController(searchResultsController: nil)
	private let queue = OperationQueueService().queue

    // MARK: - VCLifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.cellLayoutMarginsFollowReadableWidth = true
		navigationController?.navigationBar.prefersLargeTitles = false
		
		// searchBar setup
		navigationItem.searchController = searchController
		searchController.searchResultsUpdater = self
		searchController.setupSearchController()
		
		refreshControl = UIRefreshControl()
		refreshControl?.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
		refreshControl?.tintColor = UIColor(red: 91, green: 128, blue: 180, alpha: 0.5)
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		token = RealmServices.bind(tableView, to: groupsModel.groupsArray)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		token?.invalidate()
	}


    // MARK: - TableViewDataSource
	override func numberOfSections(in tableView: UITableView) -> Int {
		if searchController.isActive {
			return 2
		} else {
			return 1
		}
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if searchController.isActive {
			return section == 1 ? "Глобальный поиск" : nil
		} else {
			return nil
		}
	}
	
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if searchController.isActive {
			return (section == 0 ? filteredGroupsModel.numberOfMyGroups() : filteredGroupsModel.numberOfNotMyGroups())
		} else {
			return groupsModel.numberOfItems()
		}
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupsCell", for: indexPath) as! GroupsCell
		
		let group = searchController.isActive ? filteredGroupsModel.getGroups(indexPath)[indexPath.row] : groupsModel.getGroups()[indexPath.row]
		cell.setName(group.name)
		cell.setMembers(group.membersCount.membersString)
		
		let getImageOp = GetCacheImageOp(url: group.smallImage, cacheLifeTime: .oneMonth)
		let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
		setImageOp.addDependency(getImageOp)
		queue.addOperation(getImageOp)
		OperationQueue.main.addOperation(setImageOp)
		
		return cell
    }
	
	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		if searchController.isActive {
			return false
		} else {
			return true
		}
	}
    
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			groupsModel.deleteGroup(forIndexPath: indexPath)
		}
	}
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)
		if searchController.isActive {
			filteredGroupsModel.addGroup(forIndexPath: indexPath)
			searchController.isActive = false
			tableView.reloadData()
		}
    }
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 54.0
	}
	
	// MARK: - Functions
	@objc func handleRefresh(_ refreshControll: UIRefreshControl) {
		groupsQuery.getMyGroups { [weak self] _, errorMessage in
			if !errorMessage.isEmpty {
				print(errorMessage)
			}
			self?.refreshControl?.endRefreshing()
		}
	}

}

// MARK: - Extensions
extension GroupsTableVC: UISearchResultsUpdating {
	
	func updateSearchResults(for searchController: UISearchController) {
		if let searchText = searchController.searchBar.text {
			filteredGroupsModel.groupsArray.removeAll()
			filterContent(for: searchText)
			groupsQuery.searchAllGroup(searchString: searchText, completion: { [weak self] (groups, error) in
				if let groups = groups {
					self?.filteredGroupsModel.groupsArray.append(contentsOf: groups)
				}
				if !error.isEmpty {
					print(error)
				}
				self?.tableView.reloadData()
			})
		}
	}
	
	private func filterContent(for searchString: String) {
		do {
			let realm = try Realm()
			let groups = realm.objects(Group.self).filter("name CONTAINS[c]%@", searchString)
			filteredGroupsModel.groupsArray.append(contentsOf: groups)
		} catch {
			print(error.localizedDescription)
		}
	}
}
