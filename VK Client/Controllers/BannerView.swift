//
//  BannerView.swift
//  VK Client
//
//  Created by Тигран on 09/06/2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit
import Firebase

class BannerView: UIViewController {
	
	var bannerView: GADBannerView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = .clear
		
		bannerView = GADBannerView(adSize: kGADAdSizeBanner)
		addBannerViewToView(bannerView)
		configureBannerView()
		load()
	}
	
	func addBannerViewToView(_ bannerView: GADBannerView) {
		bannerView.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(bannerView)
		view.addConstraints(
			[NSLayoutConstraint(item: bannerView,
								attribute: .bottom,
								relatedBy: .equal,
								toItem: view.safeAreaLayoutGuide,
								attribute: .bottom,
								multiplier: 1,
								constant: 0),
			 NSLayoutConstraint(item: bannerView,
								attribute: .centerX,
								relatedBy: .equal,
								toItem: view,
								attribute: .centerX,
								multiplier: 1,
								constant: 0)
			])
	}
	
	func configureBannerView() {
		bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
		bannerView.rootViewController = self
	}
	
	func load() {
		bannerView.load(GADRequest())
	}
}
