//
//  NewsFeedVC.swift
//  VK Client
//
//  Created by Тигран on 27.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit
import Firebase

final class NewsFeedVC: UITableViewController, AddPostVCDelegate {
	
	// MARK: - Variables
	private let newsFeedService = NewsFeedQueryServices.shared
	private var newsFeedModel = NewsFeedModel()
	private let dialogModel = DialogsModel.shared
	private let queue = OperationQueueService().queue
	private var heightCache: [IndexPath : CGFloat] = [:]
	private let loadingVC = UIStoryboard(name: "NewsFeed", bundle: nil).instantiateViewController(withIdentifier: "LoadingVC")
	private let bannerVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BannerVC")
	
	// MARK: - VCLifeCycles
	override func viewDidLoad() {
		super.viewDidLoad()
		
		tableView.separatorStyle = .none
		
		refreshControl = UIRefreshControl()
		refreshControl?.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
		refreshControl?.tintColor = UIColor(red: 91, green: 128, blue: 180, alpha: 0.5)
		
		SingltonService.setSingltons()
		RealmServices.loadFriends()
		RealmServices.loadGroups()
		FirebaseService.saveUserToDB()
		CloudService.shared.saveToCloud()
		UserDefaultsServices.saveToken()
		
		getNewsFeed()
		initialSetMessageBadge()
		LongPollQueryServices.shared.connectToLongPollServer {
			Timer.scheduledTimer(withTimeInterval: 30, repeats: true, block: { (_) in
				LongPollQueryServices.shared.getUpdates(completion: { updates, error in
					if let updateModel = updates, updateModel.count != 0 {
						updateModel.forEach { $0.performUpdates() }
					}
				})
			})
		}
		
		let unreadDialogsChanged = Notification.Name("unreadDialogsChanged")
		NotificationCenter.default.addObserver(self, selector: #selector(setMessageBadge), name: unreadDialogsChanged, object: nil)
		
		setupLoadingVC()
		setupBannerVC()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		tableView.reloadData()
	}
	
	// MARK: - TableViewDataSource
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return newsFeedModel.newsArray.count
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostCell
		let newsFeed = newsFeedModel.newsArray[indexPath.row]
		cell.delegate = self
		cell.index = indexPath
		setProfile(newsFeed, cell, indexPath, tableView)
		setAttachment(newsFeed, cell, indexPath, tableView)
		cell.configure(with: newsFeed)
		return cell
	}
	
	// MARK: - TableViewDelegate
	override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		let momentForLoadingMore = newsFeedModel.newsArray.count - 6
		if indexPath.row == momentForLoadingMore {
			guard let next = newsFeedModel.newsArray.last?.nextFrom else { return }
			newsFeedService.getNewsFeedFromVK(nextFrom: next, count: 15) { [weak self] (newsResults, friendsResults, groupsResults, error) in
				guard let strongSelf = self else { return }
				if let news = newsResults, let friends = friendsResults, let groups = groupsResults {
					strongSelf.newsFeedModel.addItems(news, friends, groups)
				}
				if !error.isEmpty {
					print(error)
				}
				DispatchQueue.main.async {
					strongSelf.tableView.reloadData()
				}
			}
		}
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		guard let height = heightCache[indexPath] else { return 567 }
		return height
	}
	
	// MARK: - Functions
	private func getNewsFeed() {
		self.newsFeedService.getNewsFeedFromVK(nextFrom: nil, count: 15) { [weak self] (newsResults, friendsResults, groupsResults, error) in
			guard let strongSelf = self else { return }
			strongSelf.loadingVC.willMove(toParent: nil)
			strongSelf.loadingVC.removeFromParent()
			strongSelf.loadingVC.view.removeFromSuperview()
			if let news = newsResults, let friends = friendsResults, let groups = groupsResults {
				strongSelf.newsFeedModel.clearAll()
				strongSelf.newsFeedModel.addItems(news, friends, groups)
			}
			if !error.isEmpty {
				print(error)
			}
			DispatchQueue.main.async {
				strongSelf.tableView.reloadData()
			}
		}
	}
	
	private func setProfile(_ newsFeed: NewsFeed, _ cell: PostCell, _ indexPath: IndexPath, _ tableView: UITableView) {
		if newsFeed.sourceId > 0 {
			let id = newsFeed.sourceId
			let friend = newsFeedModel.getFriend(with: id)
			let getImageOp = GetCacheImageOp(url: friend.smallImage, cacheLifeTime: .oneMonth)
			let setImageOp = SetImageToProfile(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
			cell.setName(friend.getFullName())
		} else {
			let id = -1 * newsFeed.sourceId
			let group = newsFeedModel.getGroup(with: id)
			let getImageOp = GetCacheImageOp(url: group.smallImage, cacheLifeTime: .oneMonth)
			let setImageOp = SetImageToProfile(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
			cell.setName(group.name)
		}
	}
	
	private func setAttachment(_ newsFeed: NewsFeed, _ cell: PostCell, _ indexPath: IndexPath, _ tableView: UITableView) {
		guard let attachments = newsFeed.attachments else { return }
		guard attachments.count > 0 else { return }
		switch attachments[0].type {
		case "photo":
			guard let url = attachments[0].photo?.bigImage else { return }
			let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .twoHours)
			let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
		case "video":
			guard let url = attachments[0].video?.bigImage else { return }
			let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .twoHours)
			let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
		default:
			break
		}
	}
	
	private func initialSetMessageBadge() {
		let unreadDialogs = UserDefaults.standard.integer(forKey: "Unread Dialogs")
		if unreadDialogs != 0 {
			tabBarController?.tabBar.items![1].badgeValue = "\(unreadDialogs)"
			UIApplication.shared.applicationIconBadgeNumber = unreadDialogs
		} else {
			dialogModel.getDialogs { [weak self] unreadDialogs in
				guard let strongSelf = self else { return }
				DispatchQueue.main.async {
					if unreadDialogs != 0 {
						strongSelf.tabBarController?.tabBar.items![1].badgeValue = "\(unreadDialogs)"
						UIApplication.shared.applicationIconBadgeNumber = unreadDialogs
					}
				}
			}
		}
	}
	
	@objc func setMessageBadge() {
		DispatchQueue.main.async {
			var badgeNumber = UIApplication.shared.applicationIconBadgeNumber
			let unreadDialogs = UserDefaults.standard.integer(forKey: "Unread Dialogs")
			badgeNumber += unreadDialogs
			if unreadDialogs == 0 {
				self.tabBarController?.tabBar.items?[1].badgeValue = nil
				UIApplication.shared.applicationIconBadgeNumber = badgeNumber
			} else {
				self.tabBarController?.tabBar.items?[1].badgeValue = "\(unreadDialogs)"
				UIApplication.shared.applicationIconBadgeNumber = badgeNumber
			}
		}
	}
	
	private func setupLoadingVC() {
		loadingVC.view.frame = tableView.frame
		addChild(loadingVC)
		view.addSubview(loadingVC.view)
		loadingVC.didMove(toParent: self)
	}
	
	private func setupBannerVC() {
		let bannerVCSize = CGSize(width: tableView.frame.width, height: 50)
		let bannerVCOrigin = CGPoint(x: 0, y: (tableView.frame.height - (tabBarController?.tabBar.frame.height)! - 50))
		bannerVC.view.frame = CGRect(origin: bannerVCOrigin, size: bannerVCSize)
		tabBarController?.addChild(bannerVC)
		tabBarController?.view.addSubview(bannerVC.view)
		bannerVC.didMove(toParent: tabBarController)
	}
	
	@objc func handleRefresh(_ refreshControll: UIRefreshControl) {
		self.newsFeedService.getNewsFeedFromVK(nextFrom: nil, count: 15) { [weak self] (newsResults, friendsResults, groupsResults, error) in
			guard let strongSelf = self else { return }
			if let news = newsResults, let friends = friendsResults, let groups = groupsResults {
				strongSelf.newsFeedModel.clearAll()
				strongSelf.newsFeedModel.addItems(news, friends, groups)
			}
			if !error.isEmpty {
				print(error)
			}
			strongSelf.refreshControl?.endRefreshing()
			DispatchQueue.main.async {
				strongSelf.tableView.reloadData()
			}
		}
	}
	
	func overlayBlurredBackgroudView() {
		let blurredBackgroundView = UIVisualEffectView()
		blurredBackgroundView.frame = view.frame
		view.addSubview(blurredBackgroundView)
		tabBarController?.tabBar.isHidden = true
	}
	
	func removeBlurredBackgroundView() {
		for subview in view.subviews {
			if subview.isKind(of: UIVisualEffectView.self) {
				subview.removeFromSuperview()
			}
		}
		tabBarController?.tabBar.isHidden = false
	}
	
	// MARK: - Navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showNews" {
			guard let indexPath = tableView.indexPathForSelectedRow else { return }
			let newsFeed = newsFeedModel.newsArray[indexPath.row]
			let detailNewsVC = segue.destination as! DetailNewsFeedTableVC
			if newsFeed.sourceId > 0 {
				let friend = newsFeedModel.getFriend(with: newsFeed.sourceId)
				detailNewsVC.friend = friend
			} else {
				let group = newsFeedModel.getGroup(with: (newsFeed.sourceId * -1))
				detailNewsVC.group = group
			}
			detailNewsVC.newsFeed = newsFeed
		} else if segue.identifier == "addPost" {
			self.definesPresentationContext = true
			self.providesPresentationContextTransitionStyle = true
			
			self.overlayBlurredBackgroudView()
			
			if let addPostVC = segue.destination as? AddPostVC {
				addPostVC.delegate = self
				addPostVC.modalPresentationStyle = .overFullScreen
			}
		}
	}
}

// MARK: - Extensions
extension NewsFeedVC: HeightDelegate {
	func setHeight(_ height: CGFloat, _ index: IndexPath) {
		heightCache[index] = height
		tableView.beginUpdates()
		tableView.reloadRows(at: [index], with: .automatic)
		tableView.endUpdates()
	}
}
