//
//  SettingsTableVC.swift
//  VK Client
//
//  Created by Тигран on 21.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

protocol SettingsTableVCDelegate: class {
	func setSettings(isPrivate: Bool, at time: Date?)
}

class SettingsTableVC: UITableViewController {
	
	// MARK: - Variables
	weak var delegate: SettingsTableVCDelegate?
	var postDate: Date?
	var isPrivate = false
	
	// MARK: - Outlets
	@IBOutlet weak var friendsOnlySwitch: UISwitch!
	@IBOutlet weak var defferedDateSwitch: UISwitch!
	@IBOutlet weak var defferedDate: UILabel!
	@IBOutlet weak var dateSelector: UIDatePicker!
	
	// MARK: - ViewControllerLifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
		
		tableView.isScrollEnabled = false
		
		dateSelector.setDate(Date() + 10800, animated: false)
		dateSelector.locale = Locale(identifier: "ru_RU")
		
		if isPrivate {
			friendsOnlySwitch.isOn = true
		} else {
			friendsOnlySwitch.isOn = false
		}
		
		if postDate != nil {
			defferedDateSwitch.isOn = true
			dateSelector.date = postDate!
		} else {
			defferedDateSwitch.isOn = false
		}
		
		defferedDate.text = DateService.shared.getPostDate(with: dateSelector.date)
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		UIApplication.shared.statusBarStyle = .lightContent
	}

    // MARK: - TableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return section == 0 ? 1 : 3
    }
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath == IndexPath(row: 1, section: 1) || indexPath == IndexPath(row: 2, section: 1) {
			return defferedDateSwitch.isOn ? tableView.estimatedRowHeight : 0.0
		}
		return tableView.estimatedRowHeight
	}
	
	// MARK: - Actions
	@IBAction func defferedEntrySelector(_ sender: UISwitch) {
		tableView.reloadData()
	}
	
	@IBAction func dateSelectorDidChange(_ sender: UIDatePicker) {
		if dateSelector.date < Date() {
			dateSelector.setDate(Date(), animated: true)
			defferedDate.text = DateService.shared.getPostDate(with: dateSelector.date)
		} else {
			defferedDate.text = DateService.shared.getPostDate(with: dateSelector.date)
		}
	}
	
	@IBAction func doneAction(_ sender: UIBarButtonItem) {
		UIApplication.shared.statusBarStyle = .default
		dismiss(animated: true) { [weak self] in
			guard let strongSelf = self else { return }
			if strongSelf.defferedDateSwitch.isOn {
				strongSelf.delegate?.setSettings(isPrivate: strongSelf.friendsOnlySwitch.isOn, at: strongSelf.dateSelector.date)
			} else {
				strongSelf.delegate?.setSettings(isPrivate: strongSelf.friendsOnlySwitch.isOn, at: nil)
			}
		}
	}
	
}
