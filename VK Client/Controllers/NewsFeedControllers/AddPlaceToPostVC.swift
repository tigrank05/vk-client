//
//  AddPlaceToPostVC.swift
//  VK Client
//
//  Created by Тигран on 14.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import MapKit
import CoreLocation

protocol AddPlaceToPostVCDelegate: class {
	func setLocation(_ location: Location)
}

class AddPlaceToPostVC: UIViewController {
	
	// MARK: - Variables
	private let locationManager = CLLocationManager()
	private var locationModel = LocationModel()
	private let queue = OperationQueueService().queue
	
	weak var delegate: AddPlaceToPostVCDelegate?
	
	// MARK: - Outlets
	@IBOutlet weak private var map: MKMapView!
	@IBOutlet weak private var tableView: UITableView!
	
	// MARK: - ViewControllerLifeCycles
	override func viewDidLoad() {
        super.viewDidLoad()
		
		tableView.delegate = self
		tableView.dataSource = self
		
		locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyBest
		locationManager.requestWhenInUseAuthorization()
		locationManager.startUpdatingLocation()
		
		setCurrentLocation()
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		UIApplication.shared.statusBarStyle = .lightContent
	}
	
	// MARK: - Private Functions
	fileprivate func setCurrentLocation() {
		if let currentLocation = locationManager.location?.coordinate {
			let coordinate = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
			let coder = CLGeocoder()
			
			coder.reverseGeocodeLocation(coordinate) { [weak self] places, error in
				guard let strongSelf = self else { return }
				if let myPlace = places?.first {
					let street = myPlace.thoroughfare
					let buildingNumber = myPlace.subThoroughfare
					var address = ""
					if street != nil && buildingNumber != nil {
						address = "\(street!), \(buildingNumber!)"
					} else {
						address = "\(street!)"
					}
					let myLocation = Location(title: "Текущее месторасположение", latitude: currentLocation.latitude, longtitude: currentLocation.longitude, address: address)
					strongSelf.locationModel.setCurrentLocation(with: myLocation)
					DispatchQueue.main.async {
						strongSelf.tableView.reloadData()
					}
				}
			}
		}
	}
	
	// MARK: - Actions
	@IBAction func cancelPressed(_ sender: UIBarButtonItem) {
		dismiss(animated: true, completion: nil)
	}
}

extension AddPlaceToPostVC: UITableViewDelegate, UITableViewDataSource {
	
	// MARK: - UITableViewDataSource
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return locationModel.count()
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "locationCell", for: indexPath) as! LocationCell
		let location = locationModel.getLocation(for: indexPath)
		cell.setNameAndAddress(location.title, address: location.getAddress())
		
		let getImageOp = GetCacheImageOp(url: location.icon, cacheLifeTime: .oneMonth)
		let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
		setImageOp.addDependency(getImageOp)
		queue.addOperation(getImageOp)
		OperationQueue.main.addOperation(setImageOp)
		
		return cell
	}
	
	// MARK: - UITableViewDelegate
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let location = locationModel.getLocation(for: indexPath)
		delegate?.setLocation(location)
		tableView.deselectRow(at: indexPath, animated: false)
		UIApplication.shared.statusBarStyle = .default
		dismiss(animated: true, completion: nil)
	}
}

extension AddPlaceToPostVC: CLLocationManagerDelegate {
	
	// MARK: - CLLocationManagerDelegate
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if let currentLocation = locations.last?.coordinate {
			
			let currentRadius: CLLocationDistance = 1000
			let currentRegion = MKCoordinateRegion(center: currentLocation, latitudinalMeters: currentRadius * 2, longitudinalMeters: currentRadius * 2)
			map.setRegion(currentRegion, animated: false)
			map.showsUserLocation = true
			
			var previousLocation = CLLocationCoordinate2D()
			let previousLocationIndex = locations.count - 2
			if previousLocationIndex >= 0 {
				previousLocation = locations[previousLocationIndex].coordinate
			}
			if currentLocation.latitude != previousLocation.latitude || currentLocation.longitude != previousLocation.longitude {
				locationModel.clearAllButCurrent()
				
				LocationQueryServices.shared.getLocations(for: currentLocation) { [weak self] (myLocations, error) in
					guard let strongSelf = self else { return }
					if let locationsList = myLocations {
						strongSelf.locationModel.addLocations(locationsList)
						DispatchQueue.main.async {
							strongSelf.tableView.reloadData()
						}
					}
					if !error.isEmpty {
						print(error)
					}
				}
			}
		}
	}
}
