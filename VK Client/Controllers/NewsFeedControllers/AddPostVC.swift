//
//  AddPostVC.swift
//  VK Client
//
//  Created by Тигран on 14.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

protocol AddPostVCDelegate: class {
	func removeBlurredBackgroundView()
}

class AddPostVC: UIViewController {
	
	// MARK: - Variables
	var location = Location()
	var publishedPhotos = [String : Photo]() {
		didSet {
			DispatchQueue.main.async {
				if self.messageTextView.text == "" && self.publishedPhotos.count == 0 {
					self.postButton.tintColor = UIColor.lightGray
				} else {
					self.postButton.tintColor = UIColor.blue
				}
			}
		}
	}
	var photosName = [String]()
	var images = [String : UIImage]()
	private var isPrivate = false {
		didSet {
			if isPrivate {
				lockIcon.isHidden = false
			} else {
				lockIcon.isHidden = true
			}
		}
	}
	private var postDate: Date? = nil
	
	weak var delegate: AddPostVCDelegate?
	
	// MARK: - Outlets
	@IBOutlet weak private var messageTextView: UITextView!
	@IBOutlet weak private var placeholderField: UITextField!
	@IBOutlet weak var bottomConstraint: NSLayoutConstraint!
	@IBOutlet weak var addressLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var postButton: UIBarButtonItem! {
		didSet {
			if messageTextView.text == "" && publishedPhotos.count == 0 {
				postButton.tintColor = UIColor.lightGray
			} else {
				postButton.tintColor = UIColor.blue
			}
		}
	}
	@IBOutlet weak var addPhotoButton: UIButton!
	@IBOutlet weak var photoCollectionView: UICollectionView!
	@IBOutlet weak var lockIcon: UIImageView!
	
	// MARK: - ViewControllerLifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
		
		messageTextView.delegate = self
		
		photoCollectionView.delegate = self
		photoCollectionView.dataSource = self
		
		lockIcon.isHidden = true
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		UIApplication.shared.statusBarStyle = .default
		messageTextView.becomeFirstResponder()
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasHidden), name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		UIApplication.shared.statusBarStyle = .lightContent
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
		NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
	}
	
	// MARK: - Functions
	@objc func keyboardWasShown(notification: Notification) {
		let info = notification.userInfo! as NSDictionary
		let kbSize = (info.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue).cgRectValue.size
		bottomConstraint.constant = kbSize.height * -1
	}
	
	@objc func keyboardWasHidden(notification: Notification) {
		bottomConstraint.constant = 0
	}
	
	// MARK: - Actions
	@IBAction func cancelPressed(_ sender: UIBarButtonItem) {
		dismiss(animated: true, completion: nil)
		delegate?.removeBlurredBackgroundView()
	}
	
	@IBAction func postPressed(_ sender: UIBarButtonItem) {
		guard let message = messageTextView.text, message != "" else { return }
		var photos: [Photo]?
		for (_, value) in publishedPhotos {
			photos?.append(value)
		}
		NewsFeedQueryServices.shared.postOnWall(with: message, lat: location.latitude, long: location.longitude, placeId: location.id, photos: photos, isPrivate: isPrivate, publishDate: postDate)
		dismiss(animated: true, completion: nil)
		delegate?.removeBlurredBackgroundView()
	}
	
	@IBAction func addPhoto(_ sender: UIButton) {
		let photoSourceRequestController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		let cameraAction = UIAlertAction(title: "Камера", style: .default, handler: { [weak self]  _ in
			guard let strongSelf = self else { return }
			if UIImagePickerController.isSourceTypeAvailable(.camera) {
				let imagePicker = UIImagePickerController()
				imagePicker.delegate = strongSelf
				imagePicker.allowsEditing = false
				imagePicker.sourceType = .camera
				
				strongSelf.present(imagePicker, animated: true)
			}
		})
		
		let photoLibraryAction = UIAlertAction(title: "Фото/Видео", style: .default, handler: { [weak self] _ in
			guard let strongSelf = self else { return }
			if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
				let imagePicker = UIImagePickerController()
				imagePicker.delegate = strongSelf
				imagePicker.allowsEditing = false
				imagePicker.sourceType = .photoLibrary
				
				strongSelf.present(imagePicker, animated: true)
			}
		})
		
		let cancelAction = UIAlertAction(title: "Отменить", style: .cancel)
		
		photoSourceRequestController.addAction(cameraAction)
		photoSourceRequestController.addAction(photoLibraryAction)
		photoSourceRequestController.addAction(cancelAction)
		
		present(photoSourceRequestController, animated: true)
	}
	
	@IBAction func addAttachment(_ sender: UIButton) {
	}
	
	// MARK: - Segue
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showLocationSelectionVC" {
			let navVC = segue.destination as! UINavigationController
			let addPlaceVC = navVC.viewControllers.first as! AddPlaceToPostVC
			addPlaceVC.delegate = self
		}else if segue.identifier == "showSettings" {
			let navVC = segue.destination as! UINavigationController
			let settingVC = navVC.viewControllers.first as! SettingsTableVC
			settingVC.postDate = postDate
			settingVC.isPrivate = isPrivate
			settingVC.delegate = self
		} else if segue.identifier == "showFriends" {
			let navVC = segue.destination as! UINavigationController
			let addFriendVC = navVC.viewControllers.first as! AddFriendVC
			addFriendVC.delegate = self
		}
	}
}

extension AddPostVC: UICollectionViewDelegate, UICollectionViewDataSource {
	
	// MARK: - UICollectionViewDataSource
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return publishedPhotos.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = photoCollectionView.dequeueReusableCell(withReuseIdentifier: "photosCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
		let photoName = photosName[indexPath.item]
		cell.delegate = self
		cell.photo.image = images[photoName]
		cell.photoName = photoName
		return cell
	}
}

extension AddPostVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	// MARK: - UIImagePickerControllerDelegate
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage, let imageUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL {
			let imageName = imageUrl.lastPathComponent
			let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
			let photoURL = URL(fileURLWithPath: documentDirectory)
			let localPath = photoURL.appendingPathComponent(imageName)
			if !FileManager.default.fileExists(atPath: localPath.path) {
				do {
					try image.jpegData(compressionQuality: 1.0)?.write(to: localPath)
					print("file saved")
				}catch {
					print("error saving file")
				}
			}
			else {
				print("file already exists")
			}
			
			PhotosQueryServices.shared.uploadWallPhotoToServer(localPath, with: imageName, latitude: location.latitude, longitude: location.longitude, completion: { [weak self] photos, error in
				guard let strongSelf = self else { return }
				if let photos = photos {
					for photo in photos {
						let photoName = image.hashValue.description
						strongSelf.photosName.append(photoName)
						strongSelf.publishedPhotos[photoName] = photo
						strongSelf.images[photoName] = image
					}
				}
				if !error.isEmpty {
					print(error)
				}
				if strongSelf.publishedPhotos.count >= 5 {
					strongSelf.addPhotoButton.isEnabled = false
				}
				DispatchQueue.main.async {
					picker.dismiss(animated: true, completion: nil)
					strongSelf.photoCollectionView.reloadData()
				}
			})
		}
	}
}

extension AddPostVC: UITextViewDelegate {
	
	// MARK: - UITextViewDelegate
	func textViewDidChange(_ textView: UITextView) {
		if textView.text == "" && publishedPhotos.count == 0 {
			placeholderField.isHidden = false
			postButton.tintColor = UIColor.lightGray
		} else {
			placeholderField.isHidden = true
			postButton.tintColor = UIColor.blue
		}
	}
}

extension AddPostVC: AddPlaceToPostVCDelegate {
	
	// MARK: - AddPlaceToPostVCDelegate
	func setLocation(_ location: Location) {
		addressLabel.text = "\(location.title)"
		self.location = location
	}
}

extension AddPostVC: SettingsTableVCDelegate {
	
	// MARK: - SettingsTableVCDelegate
	func setSettings(isPrivate: Bool, at time: Date?) {
		self.isPrivate = isPrivate
		if let date = time {
			postDate = date
			dateLabel.text = DateService.shared.getPublishDate(with: date)
		}
	}
}

extension AddPostVC: PhotoCollectionCellDelegate {
	
	// MARK: - PhotoCollectionCellDelegate
	func deleteCollectionViewCell(withImage name: String) {
		var keyPossition = 0
		for i in keyPossition..<photosName.count {
			if photosName[i] == name {
				keyPossition = i
				break
			}
		}
		
		publishedPhotos.removeValue(forKey: name)
		images.removeValue(forKey: name)
		photosName.remove(at: keyPossition)
		photoCollectionView.deleteItems(at: [IndexPath(item: keyPossition, section: 0)])
	}
}

extension AddPostVC: AddFriendVCDelegate {
	
	// MARK: - AddFriendVCDelegate
	func addFriend(with id: Int, name: String) {
		placeholderField.isHidden = true
		let addFriendToPostString = "*id\(id.description) (\(name))"
		if messageTextView.text == "" {
			messageTextView.text = addFriendToPostString
		} else {
			messageTextView.text = messageTextView.text + "\n\(addFriendToPostString)"
		}
	}
}

