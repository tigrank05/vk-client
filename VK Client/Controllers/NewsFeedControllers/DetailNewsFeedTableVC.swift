//
//  DetailNewsFeedTableVC.swift
//  VK Client
//
//  Created by Тигран on 18.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class DetailNewsFeedTableVC: UITableViewController {
	
	var newsFeed: NewsFeed?
	var friend: Friend?
	var group: Group?
	
	private let queue = OperationQueueService().queue
	private var cellHeight: CGFloat = 567

    override func viewDidLoad() {
        super.viewDidLoad()
		tableView.separatorStyle = .none
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
	
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if let newsFeed = newsFeed {
			let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostCell
			cell.detailDelegate = self
			setProfile(cell, indexPath, tableView)
			setAttachment(newsFeed, cell, indexPath, tableView)
			cell.configureDetailView(with: newsFeed)
			return cell
		} else {
			let cell = tableView.dequeueReusableCell(withIdentifier: "", for: indexPath)
			return cell
		}
    }
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return cellHeight
	}
	
	private func setProfile(_ cell: PostCell, _ indexPath: IndexPath, _ tableView: UITableView) {
		if let friend = friend {
			let getImageOp = GetCacheImageOp(url: friend.smallImage, cacheLifeTime: .oneMonth)
			let setImageOp = SetImageToProfile(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
			cell.setName(friend.getFullName())
		}
		if let group = group {
			let getImageOp = GetCacheImageOp(url: group.smallImage, cacheLifeTime: .oneMonth)
			let setImageOp = SetImageToProfile(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
			cell.setName(group.name)
		}
	}
	
	private func setAttachment(_ newsFeed: NewsFeed, _ cell: PostCell, _ indexPath: IndexPath, _ tableView: UITableView) {
		guard let attachments = newsFeed.attachments else { return }
		guard attachments.count > 0 else { return }
		switch attachments[0].type {
		case "photo":
			guard let url = attachments[0].photo?.bigImage else { return }
			let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .twoHours)
			let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
		case "video":
			guard let url = attachments[0].video?.bigImage else { return }
			let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .twoHours)
			let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
		default:
			break
		}
	}
}

extension DetailNewsFeedTableVC: DetailHeightDelegate {
	func setHeight(_ height: CGFloat) {
		cellHeight = height
		tableView.reloadData()
	}
}
