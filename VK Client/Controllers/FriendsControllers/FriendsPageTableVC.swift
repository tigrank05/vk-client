//
//  FriendsPageTableVC.swift
//  VK Client
//
//  Created by Тигран on 17.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class FriendsPageTableVC: UITableViewController {
	
	var id: String? {
		didSet {
			guard let id = id else { return }
			UserQueryServices.shared.getUser(with: id) { [weak self] user, error in
				guard let strongSelf = self else { return }
				if let user = user {
					strongSelf.friend = user
				}
				if !error.isEmpty {
					print(error)
				}
			}
			
			NewsFeedQueryServices.shared.getWallFromVK(for: id) { [weak self] (newsResults, friendsResults, groupsResults, error) in
				guard let strongSelf = self else { return }
				if let news = newsResults, let friends = friendsResults, let groups = groupsResults {
					strongSelf.wallPosts.clearAll()
					strongSelf.wallPosts.addItems(news, friends, groups)
					DispatchQueue.main.async {
						strongSelf.tableView.reloadData()
					}
				}
				if !error.isEmpty {
					print(error)
				}
			}
		}
	}
	
	var friend: Friend? {
		didSet {
			guard let friend = friend else { return }
			FriendsQueryServices.shared.getFriends(for: friend) { [weak friend] friends, error in
				guard let strongFriend = friend else { return }
				if let friends = friends {
					strongFriend.friends.append(contentsOf: friends)
				}
				if !error.isEmpty {
					print(error)
				}
			}
			
			GroupsQueryServices.shared.getGroups(for: friend) { [weak friend] groups, error in
				guard let strongFriend = friend else { return }
				if let groups = groups {
					strongFriend.groups.append(contentsOf: groups)
				}
				if !error.isEmpty {
					print(error)
				}
			}
			
			PhotosQueryServices.shared.getWallPhotos(for: friend, count: .max) { [weak friend] photos, error in
				guard let strongFriend = friend else { return }
				if let photos = photos {
					strongFriend.photos.append(contentsOf: photos)
				}
				if !error.isEmpty {
					print(error)
				}
			}
			
			VideoQueryServices.shared.getVideos(for: friend) { [weak friend] videosCount, error in
				guard let strongFriend = friend else { return }
				if let videos = videosCount {
					strongFriend.videosCount = videos
				}
				if !error.isEmpty {
					print(error)
				}
			}
		}
	}
	
	private var heightCache: [IndexPath : CGFloat] = [:]
	private var storedOffsets: [Int: CGFloat] = [:]
	private let queue = OperationQueueService().queue
	
	private var wallPosts = NewsFeedModel()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wallPosts.newsArray.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if indexPath.row == 0 {
			guard let friend = friend else { return UITableViewCell() }
			let cell = tableView.dequeueReusableCell(withIdentifier: "friendsPageHeader", for: indexPath) as! FriendsPageHeaderTableCell
			cell.delegate = self
			cell.friendId = friend.id
			cell.configure(with: friend)
			
			let getImageOp = GetCacheImageOp(url: friend.bigImage, cacheLifeTime: .oneMonth)
			let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
			
			return cell
		} else {
			let cell = tableView.dequeueReusableCell(withIdentifier: "friendsWallCell", for: indexPath) as! PostCell
			let post = wallPosts.newsArray[indexPath.row - 1]
			cell.delegate = self
			cell.index = indexPath
			setProfile(post, cell, indexPath, tableView)
			setAttachment(post, cell, indexPath, tableView)
			cell.configure(with: post)
			return cell
		}
    }
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		guard let height = heightCache[indexPath] else { return 300 }
		return height
	}
	
	private func setProfile(_ post: NewsFeed, _ cell: PostCell, _ indexPath: IndexPath, _ tableView: UITableView) {
		guard let friend = friend else { return }
		let getImageOp = GetCacheImageOp(url: friend.smallImage, cacheLifeTime: .oneMonth)
		let setImageOp = SetImageToProfile(cell: cell, indexPath: indexPath, tableView: tableView)
		setImageOp.addDependency(getImageOp)
		queue.addOperation(getImageOp)
		OperationQueue.main.addOperation(setImageOp)
		cell.setName(friend.getFullName())
	}
	
	private func setAttachment(_ post: NewsFeed, _ cell: PostCell, _ indexPath: IndexPath, _ tableView: UITableView) {
		guard let attachments = post.attachments, attachments.count > 0 else { return }
		guard let url = attachments[0].photo?.bigImage else { return }
		let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .twoHours)
		let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
		setImageOp.addDependency(getImageOp)
		queue.addOperation(getImageOp)
		OperationQueue.main.addOperation(setImageOp)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		guard let friend = friend else { return }
		if segue.identifier == "showPhotosSegue" {
			let collectionVC = segue.destination as! FriendsPhotoCollectionVC
			collectionVC.photos = friend.photos
		}
	}
	
	@IBAction func sendMessage(_ sender: UIButton) {
		let messageStoryboard = UIStoryboard.init(name: "Messages", bundle: nil)
		let directMessageVC = messageStoryboard.instantiateViewController(withIdentifier: "messagesVC") as! DirectMessagesVC
		directMessageVC.userId = friend?.id
		directMessageVC.title = friend?.getFullName()
//		performSegue(withIdentifier: "openMessages", sender: self)
	}
}

extension FriendsPageTableVC: HeightDelegate {
	func setHeight(_ height: CGFloat, _ index: IndexPath) {
		heightCache[index] = height
		tableView.beginUpdates()
		tableView.reloadRows(at: [index], with: .automatic)
		tableView.endUpdates()
	}
}
