//
//  FriendsTableVC.swift
//  VK Client
//
//  Created by Тигран on 21.02.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import RealmSwift

final class FriendsTableVC: UITableViewController {

    //    MARK: - Variables
	private var searchController = UISearchController(searchResultsController: nil)
	private let friendsModel = RealmFriendsModel()
	private let filteredFriendsModel = NativeFriendsModel()
	private var token: NotificationToken?
	private let queue = OperationQueueService().queue
	
	// MARK: - VCLifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
		
		tableView.sectionIndexColor? = UIColor(red: 91, green: 128, blue: 180, alpha: 0.5)
		tableView.sectionIndexBackgroundColor = .white
		navigationController?.navigationBar.prefersLargeTitles = false
		
		refreshControl = UIRefreshControl()
		refreshControl?.addTarget(self, action: #selector(self.handleRefresh(_:)), for: .valueChanged)
		refreshControl?.tintColor = UIColor(red: 91, green: 128, blue: 180, alpha: 0.5)
		
		navigationItem.searchController = searchController
		searchController.searchResultsUpdater = self
		searchController.setupSearchController()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		searchController.hidesNavigationBarDuringPresentation = false
		token = RealmServices.bind(tableView, to: friendsModel.getFriends())
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		token?.invalidate()
		searchController.searchBar.text = ""
		searchController.dismiss(animated: false)
	}

    // MARK: - TablViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
		if searchController.isActive {
			return filteredFriendsModel.getNumberOfSections()
		} else {
			return friendsModel.getNumberOfSections()
		}
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if searchController.isActive {
			return filteredFriendsModel.getTitlesOfSections()[section]
		} else {
			return friendsModel.getTitlesOfSections()[section]
		}
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if searchController.isActive {
			return filteredFriendsModel.getNumberOfRowsInSections(for: section)
		} else {
			return friendsModel.getNumberOfRowsInSections(for: section)
		}
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsCell", for: indexPath) as! FriendsTableCell
        
		let friend = searchController.isActive ? filteredFriendsModel.getFriends(forSection: indexPath.section)[indexPath.row] : friendsModel.getFriends(forSection: indexPath.section)[indexPath.row]
		cell.setName(friend.firstName, friend.lastName)
		cell.setOnlineStatus(friend.online)
		
		let getImageOp = GetCacheImageOp(url: friend.smallImage, cacheLifeTime: .oneMonth)
		let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
		setImageOp.addDependency(getImageOp)
		queue.addOperation(getImageOp)
		OperationQueue.main.addOperation(setImageOp)
		
		return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)
	}
	
	override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
		if searchController.isActive {
			return filteredFriendsModel.getTitlesOfSections()
		} else {
			return friendsModel.getTitlesOfSections()
		}
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 54.0
	}
	
	// MARK: - Functions
	@objc func handleRefresh(_ refreshControll: UIRefreshControl) {
		FriendsQueryServices.shared.getFriends { _, error in
			if !error.isEmpty {
				print(error)
			}
		}
		refreshControl?.endRefreshing()
	}
	
	// MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "showFriendPage" else { return }
		guard let indexPath = tableView.indexPathForSelectedRow else { return }
		let friend = searchController.isActive ? (filteredFriendsModel.getFriends(forSection: indexPath.section)[indexPath.row]) : (friendsModel.getFriends(forSection: indexPath.section)[indexPath.row])
		guard let friendsPageVC = segue.destination as? FriendsPageTableVC else { return }
		friendsPageVC.id = friend.id.description
		friendsPageVC.title = friend.firstName
	}
	
}

extension FriendsTableVC: UISearchResultsUpdating {
	
	// MARK: - UISearchResultsUpdating
	func updateSearchResults(for searchController: UISearchController) {
		if let searchText = searchController.searchBar.text {
			filteredFriendsModel.clearAll()
			filteredFriendsModel.append(contentsOf: RealmServices.filterFriends(with: searchText))
			self.tableView.reloadData()
		}
	}
}
