//
//  EnlargeFriendsPhotoVC.swift
//  VK Client
//
//  Created by Тигран on 24.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class EnlargeFriendsPhotoVC: UIViewController {
	
	var photoUrl: String?
	private let queue = OperationQueueService().queue
	
	@IBOutlet weak private var photo: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
		
		view.backgroundColor = UIColor.black
		photo.backgroundColor = UIColor.black
		
		guard let url = photoUrl else { return }
		let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .oneMonth)
		getImageOp.completionBlock = {
			OperationQueue.main.addOperation {
				self.photo.image = getImageOp.outputImage
			}
		}
		queue.addOperation(getImageOp)
		
		let hideControllerGesture = UITapGestureRecognizer(target: self, action: #selector(cancelButtonPressed))
		hideControllerGesture.cancelsTouchesInView = false
		view.addGestureRecognizer(hideControllerGesture)
    }
	
	@IBAction func cancelButtonPressed(_ sender: Any) {
		self.dismiss(animated: true)
	}
}
