//
//  FriendsPhotoCollectionVC.swift
//  VK Client
//
//  Created by Тигран on 23.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class FriendsPhotoCollectionVC: UICollectionViewController {
	
	var photos = [Photo]() {
		didSet {
			collectionView?.reloadData()
		}
	}
	private let queue = OperationQueueService().queue
	private let reuseIdentifier = "photoCell"
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if let layout = collectionView?.collectionViewLayout as? FriendPhotosCollectionLayout {
			layout.delegate = self
		}
	}
	
	// MARK: UICollectionViewDataSource
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return photos.count
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FriendPhotosCollectionViewCell
		let url = photos[indexPath.item].bigImage
		let getImageOp = GetCacheImageOp(url: url, cacheLifeTime: .oneMonth)
		let setImageOp = SetCollectionImageOp(cell: cell, indexPath: indexPath, collectionView: collectionView)
		setImageOp.addDependency(getImageOp)
		queue.addOperation(getImageOp)
		OperationQueue.main.addOperation(setImageOp)
		return cell
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "enlargePhoto" {
			let enlargeVC = segue.destination as! EnlargeFriendsPhotoVC
			guard let indexPath = collectionView?.indexPathsForSelectedItems else { return }
			if photos[indexPath[0].item].largeImage == "" {
				let url = photos[indexPath[0].row].bigImage
				enlargeVC.photoUrl = url
			} else {
				let url = photos[indexPath[0].item].largeImage
				enlargeVC.photoUrl = url
			}
			
		}
	}
}

extension FriendsPhotoCollectionVC: FriendPhotosCollectionLayoutDelegate {
	func collectionView(_ collectionView: UICollectionView, aspectRatioForItemAtIndexPath indexPath: IndexPath) -> CGFloat {
		guard photos[indexPath.row].height != 0 else { return 1 }
		return CGFloat(photos[indexPath.item].height / photos[indexPath.item].width)
	}
}

