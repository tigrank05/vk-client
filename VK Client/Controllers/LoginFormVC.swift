//
//  LoginFormVC.swift
//  VK Client
//
//  Created by Тигран on 16.02.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import VK_ios_sdk
import RealmSwift

final class LoginFormVC: UIViewController, VKSdkDelegate, VKSdkUIDelegate {

    // MARK: - Variables
    private let appId: String = "6351442"
    private var scope: [String]? = nil
    private var isAuthorized: Bool = false

    // MARK: - VCLifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
		
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        VKSdk.initialize(withAppId: appId).register(self)
        VKSdk.instance().uiDelegate = self
        
        VKSdk.wakeUpSession(scope) { [weak self] (state, error) in
			guard let strongSelf = self else { return }
            if state == VKAuthorizationState.authorized {
                strongSelf.startWork()
            } else if let err = error {
                print(err)
            }
        }
    }

    // MARK: - Functions
    @IBAction func registrastionPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "Ошибка", message: "К сожалению данная функция еще не реализована(((", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ок", style: .default)
        alert.addAction(okAction)
        self.present(alert, animated: true)
    }

    @IBAction func loginPressed(_ sender: UIButton) {
        scope = [VK_PER_FRIENDS, VK_PER_GROUPS, VK_PER_PHOTOS, VK_PER_MESSAGES, VK_PER_WALL, VK_PER_VIDEO]
        VKSdk.authorize(scope)
    }

    func vkSdkShouldPresent(_ controller: UIViewController!) {
        navigationController?.topViewController?.present(controller, animated: true)
    }

    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        let vkCVC = VKCaptchaViewController.captchaControllerWithError(captchaError)
        vkCVC?.present(in: navigationController?.topViewController)
    }

    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        if result.token != nil {
            self.startWork()
        } else if result.error != nil {
            self.vkSdkUserAuthorizationFailed()
        }
    }

    func vkSdkUserAuthorizationFailed() {
        navigationController?.popToRootViewController(animated: false)
    }

    func startWork() {
        self.performSegue(withIdentifier: "LoginSegue", sender: self)
    }

    // MARK: - Segues
	@IBAction func unwindToLoginFormVC(segue: UIStoryboardSegue) {
		RealmServices.clearRealm()
		VKSdk.forceLogout()
	}

}
