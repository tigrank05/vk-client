//
//  DialogsTableVC.swift
//  VK Client
//
//  Created by Тигран on 23.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import RealmSwift
import VK_ios_sdk

final class DialogsTableVC: UITableViewController {
	
	// MARK: - Variables
	private let usersModel = ProfilesModel()
	private let messagesQuery = MessagesQueryServices.shared
	private var dialogsArr = [Dialog]()
	private var token: NotificationToken?
	private let queue = OperationQueueService().queue
	private let dateService = DateService.shared

	// MARK: - VCLifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		tabBarController?.tabBar.isHidden = false
		self.getDialogs()
	}

    // MARK: - TableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dialogsArr.count
    }
	
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dialogCell", for: indexPath) as! DialogCell
		
		let user = usersModel.getProfile(byId: dialogsArr[indexPath.row].message.userId)
		let message = dialogsArr[indexPath.row].message
		cell.setName((user?.getFullName())!)
		cell.setMessage(message.body)
		cell.setDate(dateService.dateFormater.string(from: Date(timeIntervalSince1970: message.date)))
		
		let getImageOp = GetCacheImageOp(url: (user?.smallImage)!, cacheLifeTime: .oneMonth)
		let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
		setImageOp.addDependency(getImageOp)
		queue.addOperation(getImageOp)
		OperationQueue.main.addOperation(setImageOp)
		
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 54.0
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showMessage" {
			let directMessagesVC = segue.destination as! DirectMessagesVC
			directMessagesVC.messageQuery = messagesQuery
			let indexPath = tableView.indexPathForSelectedRow!
			let user = usersModel.getProfile(byId: dialogsArr[indexPath.row].message.userId)
			directMessagesVC.user = user
			directMessagesVC.title = user?.getFullName()
			directMessagesVC.dialog = dialogsArr[indexPath.row]
		}
	}
	
	// MARK: - Private
	private func getDialogs() {
		self.messagesQuery.getDialogs { [weak self] (dialogsResults, usersResults, error) in
			self?.dialogsArr.removeAll()
			self?.usersModel.profilesArray.removeAll()
			if let dialogs = dialogsResults {
				self?.dialogsArr = dialogs
			}
			if let users = usersResults {
				self?.usersModel.profilesArray = users
			}
			if !error.isEmpty {
				print(error)
			}
			DispatchQueue.main.async {
				self?.tableView.reloadData()
			}
		}
	}

}
