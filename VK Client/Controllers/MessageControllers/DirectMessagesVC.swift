//
//  DirectMessagesVC.swift
//  VK Client
//
//  Created by Тигран on 23.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import VK_ios_sdk
import JSQMessagesViewController
import Photos

final class DirectMessagesVC: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	// MARK: - Variables
	var dialog: Dialog?
	var messageQuery: MessagesQueryServices?
	private let queue = OperationQueueService().queue
	private lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
	private lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
	private let name = VKSdk.accessToken().localUser.first_name + " " + VKSdk.accessToken().localUser.last_name
	var messages = [JSQMessage]() {
		didSet {
			DispatchQueue.main.async {
				self.collectionView.reloadData()
				self.scrollToBottom(animated: false)
			}
		}
	}
	var user: Profile?
	
	// MARK: - VCLifeCycles
	override func viewDidLoad() {
		super.viewDidLoad()
		
		senderId = VKSdk.accessToken().userId
		senderDisplayName = name
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		tabBarController?.tabBar.isHidden = true
		
		guard let friend = user else { return }
		guard let dialog = dialog else { return }
		queue.maxConcurrentOperationCount = 1
		
		let downloadMessageOp = DownloadMessagesOperation(friend: friend, dialog: dialog)
		let downloadImagesOp = DownloadImagesForMessagesOperation()
		let parseMessagesOp = ParseMessageToJSQMessageOperation(friend: friend)
		let reloadControllerOp = ReloadCollectionOperation(controller: self)
		
		downloadImagesOp.addDependency(downloadMessageOp)
		parseMessagesOp.addDependency(downloadImagesOp)
		reloadControllerOp.addDependency(parseMessagesOp)
		
		queue.addOperations([downloadMessageOp, downloadImagesOp, parseMessagesOp, reloadControllerOp], waitUntilFinished: false)
	}
	
	// MARK: - CollectionViewDataSource
	override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
		return messages[indexPath.item]
	}
	
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return messages.count
	}
	
	override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
		let message = messages[indexPath.item]
		if message.senderId == senderId {
			return outgoingBubbleImageView
		} else {
			return incomingBubbleImageView
		}
	}
	
	override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
		return nil
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
		let message = messages[indexPath.item]
		
		if message.senderId == senderId {
			cell.textView?.textColor = UIColor.white
		} else {
			cell.textView?.textColor = UIColor.black
		}
		return cell
	}
	
	// MARK: - CollectionViewDelegate
	override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
		messageQuery?.sendMessage(forUser: user, message: text)
		
		let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
		messages.append(message!)
		collectionView.reloadData()
		JSQSystemSoundPlayer.jsq_playMessageSentSound()
		finishSendingMessage()
	}
	
	override func didPressAccessoryButton(_ sender: UIButton!) {
		let photoSourceRequestController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		let cameraAction = UIAlertAction(title: "Камера", style: .default, handler: { _ in
			if UIImagePickerController.isSourceTypeAvailable(.camera) {
				let imagePicker = UIImagePickerController()
				imagePicker.delegate = self
				imagePicker.allowsEditing = false
				imagePicker.sourceType = .camera
				
				self.present(imagePicker, animated: true)
			}
		})
		
		let photoLibraryAction = UIAlertAction(title: "Фото/Видео", style: .default, handler: { _ in
			if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
				let imagePicker = UIImagePickerController()
				imagePicker.delegate = self
				imagePicker.allowsEditing = false
				imagePicker.sourceType = .photoLibrary
				
				self.present(imagePicker, animated: true)
			}
		})
		
		let cancelAction = UIAlertAction(title: "Отменить", style: .cancel)
		
		photoSourceRequestController.addAction(cameraAction)
		photoSourceRequestController.addAction(photoLibraryAction)
		photoSourceRequestController.addAction(cancelAction)
		
		present(photoSourceRequestController, animated: true)
	}
	
	// MARK: - Private
	private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
		let bubbleImageFactory = JSQMessagesBubbleImageFactory()
		return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
	}
	
	private func setupIncomingBubble() -> JSQMessagesBubbleImage {
		let bubbleImageFactory = JSQMessagesBubbleImageFactory()
		return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
	}
	
	private func addMessage(withId id: String, name: String, text: String) {
		if let message = JSQMessage(senderId: id, displayName: name, text: text) {
			messages.append(message)
		}
	}
	
	// TODO: - Отладить фукционал добавления сообщения с медиафайлом. Сейчас выдает ошибку
	@objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		if let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
			let media = JSQPhotoMediaItem(image: selectedImage)
			guard let message = JSQMessage(senderId: senderId, senderDisplayName: name, date: Date(), media: media) else { return }
			messages.append(message)
			collectionView.reloadData()
		}
		dismiss(animated: true, completion: nil)
	}
}
