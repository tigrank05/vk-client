//
//  DialogsTableVC.swift
//  VK Client
//
//  Created by Тигран on 23.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import RealmSwift

final class DialogsTableVC: UITableViewController {
	
	// MARK: - Variables
	private var dialogModel = DialogsModel.shared
	private var token: NotificationToken?
	private let queue = OperationQueueService().queue
	private let dateService = DateService.shared

	// MARK: - VCLifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		dialogModel.getDialogs { [weak self] (unreadDialogs) in
			guard let strongSelf = self else { return }
			DispatchQueue.main.async {
				UserDefaults.standard.set(unreadDialogs, forKey: "Unread Dialogs")
				if unreadDialogs == 0 {
					strongSelf.tabBarController?.tabBar.items?[1].badgeValue = nil
				} else {
					strongSelf.tabBarController?.tabBar.items?[1].badgeValue = "\(unreadDialogs)"
				}
				strongSelf.tableView.reloadData()
			}
		}
	}

    // MARK: - TableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dialogModel.count()
    }
	
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dialogCell", for: indexPath) as! DialogCell
		
		let message = dialogModel.getMessage(for: indexPath)
		if message.userId > 0 {
			let user = dialogModel.usersModel.getProfile(byId: message.userId)
			cell.setName((user?.getFullName())!)
			let getImageOp = GetCacheImageOp(url: (user?.smallImage)!, cacheLifeTime: .oneMonth)
			let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
		} else {
			let group = dialogModel.groupsModel.getGroup(byId: message.userId)
			cell.setName(group.name)
			let getImageOp = GetCacheImageOp(url: group.smallImage, cacheLifeTime: .oneMonth)
			let setImageOp = SetImageOp(cell: cell, indexPath: indexPath, tableView: tableView)
			setImageOp.addDependency(getImageOp)
			queue.addOperation(getImageOp)
			OperationQueue.main.addOperation(setImageOp)
		}
		cell.setMessage(message.body)
		cell.setDate(dateService.formatLastDate(with: Date(timeIntervalSince1970: message.date)))
		cell.setUnread(dialogModel.getUnreadMessages(for: indexPath))
		
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 64.0
	}
	
	override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		let momentForLoadingMore = dialogModel.count() - 5
		if indexPath.row == momentForLoadingMore {
			guard let nextMessageId = dialogModel.getLastMessage()?.id else { return }
			MessagesQueryServices.shared.getDialogs(fromMessage: nextMessageId) { [weak self] (dialogsResults, usersResults, groupsResults, error) in
				guard let strongSelf = self else { return }
				strongSelf.dialogModel.removeLast()
				if let dialogs = dialogsResults {
					strongSelf.dialogModel.setDialogs(dialogs)
				}
				if let users = usersResults {
					strongSelf.dialogModel.usersModel.profilesArray.append(contentsOf: users)
				}
				if let groups = groupsResults {
					strongSelf.dialogModel.groupsModel.groupsArray.append(contentsOf: groups)
				}
				if !error.isEmpty {
					print(error)
				}
				DispatchQueue.main.async {
					strongSelf.tableView.reloadData()
				}
			}
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showMessage" {
			let directMessagesVC = segue.destination as! DirectMessagesVC
			let indexPath = tableView.indexPathForSelectedRow!
			let message = dialogModel.getMessage(for: indexPath)
			if message.userId > 0 {
				let user = dialogModel.usersModel.getProfile(byId: message.userId)
				directMessagesVC.userId = user?.id
				directMessagesVC.title = user?.getFullName()
			} else {
				let group = dialogModel.groupsModel.getGroup(byId: message.userId)
				directMessagesVC.userId = group.id * -1
				directMessagesVC.title = group.name
			}
			directMessagesVC.lastMessageId = dialogModel.getLastMessageId(for: indexPath)
		}
	}
}
