//
//  DirectMessagesVC.swift
//  VK Client
//
//  Created by Тигран on 23.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import JSQMessagesViewController

final class DirectMessagesVC: JSQMessagesViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	// MARK: - Variables
	var userId: Int?
	var lastMessageId: Int?
	var lastUnreadMessageId: Int?
	var messages = [JSQMessage]()
	var nativeMessages = [Message]() {
		didSet {
			for message in nativeMessages {
				if let imageUrl = message.photo?.bigImage {
					let getImageOp = GetCacheImageOp(url: imageUrl, cacheLifeTime: .oneMonth)
					getImageOp.completionBlock = {
						let media = JSQPhotoMediaItem(image: getImageOp.outputImage)
						let jsqMessage = JSQMessage(senderId: message.senderId.description, senderDisplayName: "", date: Date(timeIntervalSince1970: message.date), media: media)
						self.messages.append(jsqMessage!)
					}
					OperationQueueService().queue.addOperation(getImageOp)
				} else {
					let jsqMessage = JSQMessage(senderId: message.senderId.description, senderDisplayName: "", date: Date(timeIntervalSince1970: message.date), text: message.body)
					self.messages.append(jsqMessage!)
				}
			}
			messages.sort(by: { $0.date < $1.date })
			DispatchQueue.main.async {
				self.collectionView.reloadData()
				self.scrollToBottom(animated: false)
			}
		}
	}
	
	private lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
	private lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
	private let name = User.current.firstName + " " + User.current.lastName
	
	// MARK: - VCLifeCycles
	override func viewDidLoad() {
		super.viewDidLoad()
		
		senderId = User.current.id
		senderDisplayName = name
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		guard let userId = userId, let lastMessageId = lastMessageId else { return }
		
		MessagesQueryServices.shared.getMessages(with: userId, from: lastMessageId) { [weak self] messagesResult, error in
			guard let strongSelf = self else { return }
			if let messages = messagesResult {
				strongSelf.nativeMessages.append(contentsOf: messages)
			}
			if !error.isEmpty {
				print(error)
			}
		}
		
		let _ = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { [weak self] _ in
			guard let strongSelf = self else { return }
			guard let message = strongSelf.nativeMessages.last else { return }
			MessagesQueryServices.shared.markAsRead(from: message.id)
		}
	}
	
	// MARK: - CollectionViewDataSource
	override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
		return messages[indexPath.item]
	}
	
	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return messages.count
	}
	
	override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
		let message = messages[indexPath.item]
		if message.senderId == senderId {
			return outgoingBubbleImageView
		} else {
			return incomingBubbleImageView
		}
	}
	
	override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
		return nil
	}
	
	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
		let message = messages[indexPath.item]
		
		if message.senderId == senderId {
			cell.textView?.textColor = UIColor.white
		} else {
			cell.textView?.textColor = UIColor.black
		}
		return cell
	}
	
	override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		let momentForLoadingMore = 1
		if indexPath.item == momentForLoadingMore {
			guard let userId = userId, let lastMessage = nativeMessages.last?.id else { return }
			MessagesQueryServices.shared.getMessages(with: userId, from: lastMessage) { [weak self] messagesResult, error in
				guard let strongSelf = self else { return }
				if var messages = messagesResult {
					strongSelf.nativeMessages.removeAll()
					messages.removeLast()
					strongSelf.nativeMessages.append(contentsOf: messages)
				}
				if !error.isEmpty {
					print(error)
				}
			}
		}
	}
	
	// MARK: - CollectionViewDelegate
	override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
		MessagesQueryServices.shared.sendMessage(forUserWithId: userId, message: text)
		
		let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: (date + 14400.0), text: text)
		messages.append(message!)
		collectionView.reloadData()
		JSQSystemSoundPlayer.jsq_playMessageSentSound()
		finishSendingMessage()
	}
	
	override func didPressAccessoryButton(_ sender: UIButton!) {
		let photoSourceRequestController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		let cameraAction = UIAlertAction(title: "Камера", style: .default, handler: { [weak self] _ in
			guard let strongSelf = self else { return }
			if UIImagePickerController.isSourceTypeAvailable(.camera) {
				let imagePicker = UIImagePickerController()
				imagePicker.delegate = strongSelf
				imagePicker.allowsEditing = false
				imagePicker.sourceType = .camera
				
				strongSelf.present(imagePicker, animated: true)
			}
		})
		
		let photoLibraryAction = UIAlertAction(title: "Фото/Видео", style: .default, handler: { [weak self] _ in
			guard let strongSelf = self else { return }
			if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
				let imagePicker = UIImagePickerController()
				imagePicker.delegate = strongSelf
				imagePicker.allowsEditing = false
				imagePicker.sourceType = .photoLibrary
				
				strongSelf.present(imagePicker, animated: true)
			}
		})
		
		let cancelAction = UIAlertAction(title: "Отменить", style: .cancel)
		
		photoSourceRequestController.addAction(cameraAction)
		photoSourceRequestController.addAction(photoLibraryAction)
		photoSourceRequestController.addAction(cancelAction)
		
		present(photoSourceRequestController, animated: true)
	}
	
	// MARK: - Private
	private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
		let bubbleImageFactory = JSQMessagesBubbleImageFactory()
		return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
	}
	
	private func setupIncomingBubble() -> JSQMessagesBubbleImage {
		let bubbleImageFactory = JSQMessagesBubbleImageFactory()
		return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
	}
	
	private func addMessage(withId id: String, name: String, text: String) {
		if let message = JSQMessage(senderId: id, displayName: name, text: text) {
			messages.append(message)
		}
	}
	
	// TODO: - Отладить фукционал добавления сообщения с медиафайлом. Сейчас выдает ошибку
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
			let media = JSQPhotoMediaItem(image: selectedImage)
			guard let message = JSQMessage(senderId: senderId, senderDisplayName: name, date: Date(), media: media) else { return }
			messages.append(message)
			collectionView.reloadData()
		}
		dismiss(animated: true, completion: nil)
	}
}
