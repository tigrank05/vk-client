//
//  PhotoCollectionViewCell.swift
//  VK Client
//
//  Created by Тигран on 17.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

protocol PhotoCollectionCellDelegate: class {
	func deleteCollectionViewCell(withImage name: String)
}

class PhotoCollectionViewCell: UICollectionViewCell {
	
	weak var delegate: PhotoCollectionCellDelegate?
	
	var photoName: String!
	
	@IBOutlet weak var photo: UIImageView! {
		didSet {
			photo.layer.cornerRadius = photo.bounds.size.height / 5
			photo.layer.masksToBounds = true
		}
	}
	@IBOutlet weak private var deleteButton: UIButton! {
		didSet {
			deleteButton.layer.cornerRadius = deleteButton.bounds.size.height / 2
			deleteButton.layer.masksToBounds = true
		}
	}
	
	@IBAction func deletePhotoPressed(_ sender: UIButton) {
		delegate?.deleteCollectionViewCell(withImage: photoName)
	}
}
