//
//  PostCell.swift
//  VK Client
//
//  Created by Тигран on 27.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import AlamofireImage

final class PostCell: UITableViewCell {
    
    // MARK: - Variables
    let insets: CGFloat = 8.0
	weak var delegate: HeightDelegate?
	weak var detailDelegate: DetailHeightDelegate?
    var index: IndexPath?
    private var dateService = DateService.shared
    
    // MARK: - Outlets
    @IBOutlet weak private var separator: UIView!
    @IBOutlet weak private var viewIcon: UIImageView!
    @IBOutlet weak private var views: UILabel!
    @IBOutlet weak private var repostIcon: UIButton!
    @IBOutlet weak private var reposts: UILabel!
    @IBOutlet weak private var commentIcon: UIButton!
    @IBOutlet weak private var comments: UILabel!
    @IBOutlet weak private var likeIcon: UIButton!
    @IBOutlet weak private var likes: UILabel!
    @IBOutlet weak private var newsBody: UILabel!
    @IBOutlet weak private var dateOfCreation: UILabel!
    @IBOutlet weak private var postersName: UILabel!
    @IBOutlet weak private var newsImage: UIImageView!
    @IBOutlet weak private var postersAvatar: UIImageView!
    @IBOutlet weak private var sectionSeparator: UIView!
    
    // MARK: - Functions
    override func setNeedsLayout() {
        super.setNeedsLayout()
        [separator, viewIcon, views, repostIcon, reposts, commentIcon, comments, likeIcon, likes, newsBody, dateOfCreation, postersName, newsImage, postersAvatar, sectionSeparator].forEach { $0?.translatesAutoresizingMaskIntoConstraints = false }
        [separator, viewIcon, views, repostIcon, reposts, commentIcon, comments, likeIcon, likes, newsBody, dateOfCreation, postersName, newsImage, postersAvatar].forEach { $0.backgroundColor = .white }
        newsImage.contentMode = .scaleAspectFit
    }
    
    func configure(with news: NewsFeed) {
		setSectionSeparator()
        dateOfCreation.text = dateService.dateFormater.string(from: Date(timeIntervalSince1970: news.date))
        setDateFrame()
        newsBody.text = news.text
        newsBody.numberOfLines = 10
        setBodyFrame(maxHeight: 100)
        if let attachments = news.attachments {
            if attachments.count > 0 {
                setImageFrame(attachments[0].photo?.height, attachments[0].photo?.width)
            } else {
                setImageFrame(nil, nil)
            }
        } else {
            setImageFrame(nil, nil)
        }
        setSeparatorFrame()
        likes.text = news.likes.description
        setLikeIconFrame()
        setLikesFrame()
        comments.text = news.comments.description
        setCommentIconFrame()
        setCommentsFrame()
        reposts.text = news.reposts.description
        setRepostIconFrame()
        setRepostsFrame()
        views.text = news.views.description
        setViewsFrame()
        setViewIconFrame()
        
        let avatarHeight = postersAvatar.frame.size.height
        let bodyHeight = newsBody.frame.size.height
        let imageHeight = newsImage.frame.size.height
        let separatorHeight = separator.frame.size.height
        let iconHeight = likeIcon.frame.size.height
        let sectionSeparatorHeight = sectionSeparator.frame.size.height
        let heigth = 5 * insets + avatarHeight + bodyHeight + imageHeight + separatorHeight + iconHeight + sectionSeparatorHeight
        guard let index = index else { return }
        guard bounds.height != heigth else { return }
        delegate?.setHeight(heigth, index)
    }
    
    func configureDetailView(with news: NewsFeed) {
        sectionSeparator.frame = CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0)
        dateOfCreation.text = dateService.dateFormater.string(from: Date(timeIntervalSince1970: news.date))
        setDateFrame()
        newsBody.text = news.text
        newsBody.numberOfLines = 0
        setBodyFrame(maxHeight: CGFloat.greatestFiniteMagnitude)
        if let attachments = news.attachments {
            if attachments.count > 0 {
                setImageFrame(attachments[0].photo?.height, attachments[0].photo?.width)
            } else {
                setImageFrame(nil, nil)
            }
        } else {
            setImageFrame(nil, nil)
        }
        setSeparatorFrame()
        likes.text = news.likes.description
        setLikeIconFrame()
        setLikesFrame()
        comments.text = news.comments.description
        setCommentIconFrame()
        setCommentsFrame()
        reposts.text = news.reposts.description
        setRepostIconFrame()
        setRepostsFrame()
        views.text = news.views.description
        setViewsFrame()
        setViewIconFrame()
        
        let avatarHeight = postersAvatar.frame.size.height
        let bodyHeight = newsBody.frame.size.height
        let imageHeight = newsImage.frame.size.height
        let separatorHeight = separator.frame.size.height
        let iconHeight = likeIcon.frame.size.height
        let heigth = 5 * insets + avatarHeight + bodyHeight + imageHeight + separatorHeight + iconHeight
        guard bounds.height != heigth else { return }
        detailDelegate?.setHeight(heigth)
    }
    
    func setAvatar(_ image: UIImage?) {
        postersAvatar.image = image
        setAvatarFrame()
    }
    
    func setName(_ name: String) {
        postersName.text = name
        setNameFrame()
    }
    
    func setImage(_ image: UIImage?) {
        newsImage.image = image
    }
}

// MARK: - Frames Calculations
extension PostCell {
    private func getNameDateSize(text: String, font: UIFont) -> CGSize {
        let maxWidth = bounds.width - 5 * insets - 50
        let textBlock = CGSize(width: maxWidth, height: (66 - 3 * insets) / 2)
		let rect = text.boundingRect(with: textBlock, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let width = Double(rect.size.width)
        let height = Double(rect.size.height)
        let size = CGSize(width: ceil(width), height: ceil(height))
        return size
    }
    
    private func getBodySize(text: String, font: UIFont, maxHeight: CGFloat) -> CGSize {
        let maxWidth = bounds.width - 4 * insets
        let textBlock = CGSize(width: maxWidth, height: maxHeight)
		let rect = text.boundingRect(with: textBlock, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let width = Double(rect.size.width)
        let height = Double(rect.size.height)
        let size = CGSize(width: ceil(width), height: ceil(height))
        return size
    }
    
    private func getActionsSize(text: String, font: UIFont) -> CGSize {
        let maxWidth = (bounds.width - 10 * insets - 80) / 4
        let textBlock = CGSize(width: maxWidth, height: 20)
		let rect = text.boundingRect(with: textBlock, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let width = Double(rect.size.width)
        let height = Double(rect.size.height)
        let size = CGSize(width: ceil(width), height: ceil(height))
        return size
    }
    
    private func setSectionSeparator() {
        let size = CGSize(width: bounds.width, height: insets)
        let separatorX: CGFloat = 0.0
        let separatorY: CGFloat = 0.0
        let origin = CGPoint(x: separatorX, y: separatorY)
        sectionSeparator.frame = CGRect(origin: origin, size: size)
    }
    
    private func setAvatarFrame() {
        let imageSizeLenght: CGFloat = 50
        let imageSize = CGSize(width: imageSizeLenght, height: imageSizeLenght)
        let imageX = 2 * insets
        let imageY = sectionSeparator.frame.origin.y + sectionSeparator.frame.size.height + insets
        let imageOrigin = CGPoint(x: imageX, y: imageY)
        postersAvatar.frame = CGRect(origin: imageOrigin, size: imageSize)
        postersAvatar.image = postersAvatar.image?.af_imageRoundedIntoCircle()
    }
    
    private func setNameFrame() {
        let labelSize = getNameDateSize(text: postersName.text!, font: postersName.font)
        let labelX = 4 * insets + 50
        let labelY = postersAvatar.frame.origin.y + 25 - labelSize.height - 0.5 * insets
        let labelOrigin = CGPoint(x: labelX, y: labelY)
        postersName.frame = CGRect(origin: labelOrigin, size: labelSize)
    }
    
    private func setDateFrame() {
        let labelSize = getNameDateSize(text: dateOfCreation.text!, font: dateOfCreation.font)
        let labelX = 4 * insets + 50
        let labelY = postersName.frame.origin.y + postersName.frame.size.height + 0.5 * insets
        let labelOrigin = CGPoint(x: labelX, y: labelY)
        dateOfCreation.frame = CGRect(origin: labelOrigin, size: labelSize)
    }
    
    private func setBodyFrame(maxHeight: CGFloat) {
        let labelSize = getBodySize(text: newsBody.text!, font: newsBody.font, maxHeight: maxHeight)
        let labelX = 2 * insets
        let labelY = postersAvatar.frame.origin.y + postersAvatar.frame.size.height + insets
        let labelOrigin = CGPoint(x: labelX, y: labelY)
        newsBody.frame = CGRect(origin: labelOrigin, size: labelSize)
    }
    
    private func setImageFrame(_ imageHeight: Double?, _ imageWidth: Double?) {
        guard imageWidth != nil, imageHeight != nil else {
            newsImage.frame = CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0)
            return
        }
        guard let window = window else { return }
        var width = CGFloat(imageWidth!)
        var height = CGFloat(imageHeight!)
        let aspectRatio = height / width
        if width > bounds.width {
            width = bounds.width
            height = width * aspectRatio
        }
        if height > (window.bounds.height + 150 + 66) {
            height = window.bounds.height + 150 + 66
        }
        let imageSize = CGSize(width: ceil(width), height: ceil(height))
        let imageX = bounds.width / 2 - imageSize.width / 2
        let imageY = newsBody.frame.origin.y + newsBody.frame.size.height + insets
        let imageOrigin = CGPoint(x: imageX, y: imageY)
        newsImage.frame = CGRect(origin: imageOrigin, size: imageSize)
    }
    
    private func setSeparatorFrame() {
        let size = CGSize(width: bounds.width, height: 1.0)
        if newsImage.frame == CGRect(x: 0.0, y: 0.0, width: 0.0, height: 0.0) {
            let separatorX: CGFloat = 0.0
            let separatorY = newsBody.frame.origin.y + newsBody.frame.size.height + insets
            let origin = CGPoint(x: separatorX, y: separatorY)
            separator.frame = CGRect(origin: origin, size: size)
        } else {
            let separatorX: CGFloat = 0.0
            let separatorY = newsImage.frame.origin.y + newsImage.frame.size.height
            let origin = CGPoint(x: separatorX, y: separatorY)
            separator.frame = CGRect(origin: origin, size: size)
        }
    }
    
    private func setLikeIconFrame() {
        let iconSize = CGSize(width: 20.0, height: 20.0)
        let iconX = 2 * insets
        let iconY = separator.frame.origin.y + separator.frame.height + insets
        let iconOrigin = CGPoint(x: iconX, y: iconY)
        likeIcon.frame = CGRect(origin: iconOrigin, size: iconSize)
    }
    
    private func setLikesFrame() {
        let labelSize = getActionsSize(text: likes.text!, font: likes.font)
        let labelX = 2.5 * insets + likeIcon.frame.size.width
        let labelY = likeIcon.frame.origin.y + likeIcon.frame.size.height / 2 - labelSize.height / 2
        let labelOrigin = CGPoint(x: labelX, y: labelY)
        likes.frame = CGRect(origin: labelOrigin, size: labelSize)
        
    }
    
    private func setCommentIconFrame() {
        let iconSize = CGSize(width: 20.0, height: 20.0)
        let iconX = 3.5 * insets + likeIcon.frame.size.width + likes.frame.width
        let iconY = separator.frame.origin.y + separator.frame.height + insets
        let iconOrigin = CGPoint(x: iconX, y: iconY)
        commentIcon.frame = CGRect(origin: iconOrigin, size: iconSize)
    }
    
    private func setCommentsFrame() {
        let labelSize = getActionsSize(text: comments.text!, font: comments.font)
        let labelX = 4 * insets + 2 * likeIcon.frame.size.width + likes.frame.width
        let labelY = likeIcon.frame.origin.y + likeIcon.frame.size.height / 2 - labelSize.height / 2
        let labelOrigin = CGPoint(x: labelX, y: labelY)
        comments.frame = CGRect(origin: labelOrigin, size: labelSize)
        
    }
    
    private func setRepostIconFrame() {
        let iconSize = CGSize(width: 20.0, height: 20.0)
        let iconX = 5 * insets + 2 * likeIcon.frame.size.width + likes.frame.width + comments.frame.width
        let iconY = separator.frame.origin.y + separator.frame.height + insets
        let iconOrigin = CGPoint(x: iconX, y: iconY)
        repostIcon.frame = CGRect(origin: iconOrigin, size: iconSize)
    }
    
    private func setRepostsFrame() {
        let labelSize = getActionsSize(text: reposts.text!, font: reposts.font)
        let labelX = 5.5 * insets + 3 * likeIcon.frame.size.width + likes.frame.width + comments.frame.width
        let labelY = likeIcon.frame.origin.y + likeIcon.frame.size.height / 2 - labelSize.height / 2
        let labelOrigin = CGPoint(x: labelX, y: labelY)
        reposts.frame = CGRect(origin: labelOrigin, size: labelSize)
    }
    
    private func setViewsFrame() {
        let labelSize = getActionsSize(text: views.text!, font: views.font)
        let labelX = bounds.maxX - 2 * insets - labelSize.width
        let labelY = likeIcon.frame.origin.y + likeIcon.frame.size.height / 2 - labelSize.height / 2
        let labelOrigin = CGPoint(x: labelX, y: labelY)
        views.frame = CGRect(origin: labelOrigin, size: labelSize)
    }
    
    private func setViewIconFrame() {
        let iconSize = CGSize(width: 20.0, height: 20.0)
        let iconX = bounds.maxX - 2.5 * insets - views.frame.width - iconSize.width
        let iconY = separator.frame.origin.y + separator.frame.height + insets
        let iconOrigin = CGPoint(x: iconX, y: iconY)
        viewIcon.frame = CGRect(origin: iconOrigin, size: iconSize)
    }
}
