//
//  LocationCell.swift
//  VK Client
//
//  Created by Тигран on 14.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {
	
	@IBOutlet weak private var locationName: UILabel!
	@IBOutlet weak private var locationAddress: UILabel!
	@IBOutlet weak private var locationIcon: UIImageView! {
		didSet {
			locationIcon.layer.cornerRadius = locationIcon.bounds.width / 2
			locationIcon.layer.masksToBounds = true
		}
	}
	
	func setImage(_ image: UIImage?) {
		locationIcon.image = image
	}
	
	func setNameAndAddress(_ name: String, address: String) {
		locationName.text = name
		locationAddress.text = address
	}
	

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
