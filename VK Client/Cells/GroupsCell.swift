//
//  GroupsCell.swift
//  VK Client
//
//  Created by Тигран on 21.02.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import AlamofireImage

final class GroupsCell: UITableViewCell {
	
	//MARK: - Variables
	private let insets: CGFloat = 8.0
	
	// MARK: - Outlets
	@IBOutlet weak private var groupName: UILabel!
	@IBOutlet weak private var groupMembers: UILabel!
	@IBOutlet weak private var groupImage: UIImageView!
	
	// MARK: - Functions
	override func awakeFromNib() {
		super.awakeFromNib()
		[groupName, groupMembers, groupImage].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
		[groupName, groupMembers, groupImage].forEach { $0.backgroundColor = .white }
		setNameFrame()
		setMembersFrame()
		setImageFrame()
	}
	
	func setName(_ name: String) {
		groupName.text = name
		setNameFrame()
	}
	
	func setMembers(_ members: String) {
		groupMembers.text = members
		setMembersFrame()
	}
	
	func setImage(_ image: UIImage?) {
		groupImage.image = image
		setImageFrame()
	}
}

	// MARK: - Frames Calculations
extension GroupsCell {
	private func getLabelSize(text: String, font: UIFont) -> CGSize {
		let maxWidth = bounds.width - 5 * insets - 50
		let textBlock = CGSize(width: maxWidth, height: (54 - insets) / 2)
		let rect = text.boundingRect(with: textBlock, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
		let width = Double(rect.size.width)
		let height = Double(rect.size.height)
		let size = CGSize(width: ceil(width), height: ceil(height))
		return size
	}
	
	private func setNameFrame() {
		let labelSize = getLabelSize(text: groupName.text!, font: groupName.font)
		let labelX = 4 * insets + 50
		let labelY = bounds.midY - labelSize.height - insets * 0.5
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		groupName.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setMembersFrame() {
		let labelSize = getLabelSize(text: groupMembers.text!, font: groupMembers.font)
		let labelX = 4 * insets + 50
		let labelY = bounds.midY + insets * 0.5
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		groupMembers.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setImageFrame() {
		let imageSizeLenght: CGFloat = 50
		let imageSize = CGSize(width: imageSizeLenght, height: imageSizeLenght)
		let imageX = 2 * insets
		let imageY = bounds.midY - imageSizeLenght / 2
		let imageOrigin = CGPoint(x: imageX, y: imageY)
		groupImage.frame = CGRect(origin: imageOrigin, size: imageSize)
		groupImage.image = groupImage.image?.af_imageRoundedIntoCircle()
	}
}
