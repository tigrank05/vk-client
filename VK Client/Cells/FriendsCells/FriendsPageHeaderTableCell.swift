//
//  FriendsPageHeaderTableCell.swift
//  VK Client
//
//  Created by Тигран on 17.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import AlamofireImage

class FriendsPageHeaderTableCell: UITableViewCell {
	
	// MARK: - Variables
	private let insets: CGFloat = 8.0
	private let dateService = DateService.shared
	weak var delegate: HeightDelegate?
	var friendId: Int?
	
	// MARK: - Outlets
	@IBOutlet weak var friendsImage: UIImageView!
	@IBOutlet weak var friendsName: UILabel!
	@IBOutlet weak var friendsLastSeen: UILabel!
	@IBOutlet weak var friendsAgeAndPlace: UILabel!
	@IBOutlet weak var openInfoButton: UIButton!
	@IBOutlet weak var sendMessageButton: UIButton!
	@IBOutlet weak var becomeFriendsButton: UIButton!
	@IBOutlet weak var showPhotosButton: UIButton!
	
	// MARK: - Functions
	override func awakeFromNib() {
		super.awakeFromNib()
		[friendsImage, friendsName, friendsLastSeen, friendsAgeAndPlace, openInfoButton, sendMessageButton, becomeFriendsButton, showPhotosButton].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
		[friendsImage, friendsName, friendsLastSeen, friendsAgeAndPlace, showPhotosButton].forEach { $0.backgroundColor = .white }
	}
	
	func configure(with friend: Friend?) {
		guard let friend = friend else { return }
		if friend.isFriend {
			becomeFriendsButton.setTitle("У Вас в друзьях", for: .normal)
			becomeFriendsButton.setTitleColor(UIColor(red: 73, green: 114, blue: 167, alpha: 1), for: .normal)
			becomeFriendsButton.backgroundColor = UIColor(red: 226, green: 231, blue: 237, alpha: 1)
		}
		setImageFrame()
		setName(friend.getFullName())
		friend.online ? setLastDate("online") : setLastDate(dateService.formatLastDate(with: Date(timeIntervalSince1970: friend.lastSeen)))
		setAgeAndPlace(friend.bDate, friend.city)
		setOpenInfoButtonFrame()
		setButtonsFrame()
		setPhotosButtonLabel(friend.photos.count)
		setPhotosButtonFrame()
		let height = showPhotosButton.frame.origin.y + showPhotosButton.frame.size.height
		delegate?.setHeight(height, IndexPath(row: 0, section: 0))
	}
	
	func setImage(_ image: UIImage?) {
		friendsImage.image = image
		friendsImage.image = friendsImage.image?.af_imageRoundedIntoCircle()
	}
	
	private func setName(_ name: String) {
		friendsName.text = name
		setNameLabelFrame()
	}
	
	private func setLastDate(_ date: String) {
		friendsLastSeen.text = date
		setLastSeenLabelFrame()
	}
	
	private func setAgeAndPlace(_ bDay: String, _ city: String) {
		if bDay == "" {
			if city == "" {
				friendsAgeAndPlace.text = ""
				setAgeAndPlaceLabelFrame()
			} else {
				friendsAgeAndPlace.text = "\(city)"
				setAgeAndPlaceLabelFrame()
			}
		} else {
			let age = dateService.getAge(with: bDay)
			if city == "" {
				if age == nil {
					friendsAgeAndPlace.text = ""
				} else {
					friendsAgeAndPlace.text = "\(age!)"
				}
				setAgeAndPlaceLabelFrame()
			} else {
				if age == nil {
					friendsAgeAndPlace.text = "\(city)"
				} else {
					friendsAgeAndPlace.text = "\(age!), \(city)"
				}
				setAgeAndPlaceLabelFrame()
			}
		}
	}
	
	private func setPhotosButtonLabel(_ photosCount: Int) {
		switch photosCount {
		case 1:
			showPhotosButton.setTitle("\(photosCount.description) фотография", for: .normal)
		case 2...4:
			showPhotosButton.setTitle("\(photosCount.description) фотографии", for: .normal)
		default:
			showPhotosButton.setTitle("\(photosCount.description) фотографий", for: .normal)
		}
	}
	
	// MARK: - Frames Calculations
	private func getLabelSize(text: String, font: UIFont) -> CGSize {
		let maxWidth = bounds.width - 6 * insets - 100 - 30
		let textBlock = CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude)
		let rect = text.boundingRect(with: textBlock, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
		let width = Double(rect.size.width)
		let height = Double(rect.size.height)
		let size = CGSize(width: ceil(width), height: ceil(height))
		return size
	}
	
	private func setImageFrame() {
		let imageSizeLenght: CGFloat = 100
		let imageSize = CGSize(width: imageSizeLenght, height: imageSizeLenght)
		let imageX = 2 * insets
		let imageY = 2 * insets
		let imageOrigin = CGPoint(x: imageX, y: imageY)
		friendsImage.frame = CGRect(origin: imageOrigin, size: imageSize)
	}
	
	private func setNameLabelFrame() {
		let labelSize = getLabelSize(text: friendsName.text!, font: friendsName.font)
		let labelX = 4 * insets + 100
		let labelY = 2 * insets
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		friendsName.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setLastSeenLabelFrame() {
		let labelSize = getLabelSize(text: friendsLastSeen.text!, font: friendsLastSeen.font)
		let labelX = 4 * insets + 100
		let labelY = 2.5 * insets + friendsName.frame.size.height
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		friendsLastSeen.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setAgeAndPlaceLabelFrame() {
		let labelSize = getLabelSize(text: friendsAgeAndPlace.text!, font: friendsAgeAndPlace.font)
		let labelX = 4 * insets + 100
		let labelY = 2.5 * insets + 100 - labelSize.height
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		friendsAgeAndPlace.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setOpenInfoButtonFrame() {
		let buttonSizeLenght: CGFloat = 30
		let buttonSize = CGSize(width: buttonSizeLenght, height: buttonSizeLenght)
		let buttonX = bounds.width - 2 * insets - buttonSizeLenght
		let buttonY = 2 * insets + 35
		let buttonOrigin = CGPoint(x: buttonX, y: buttonY)
		openInfoButton.frame = CGRect(origin: buttonOrigin, size: buttonSize)
	}
	
	private func setButtonsFrame() {
		let buttonSizeLength: CGFloat = (bounds.width - 6 * insets) / 2
		let buttonSize = CGSize(width: buttonSizeLength, height: 30)
		
		let messageButtonX = 2 * insets
		let messageButtonY = friendsImage.frame.origin.y + 100 + 2 * insets
		let messageButtonOrigin = CGPoint(x: messageButtonX, y: messageButtonY)
		sendMessageButton.frame = CGRect(origin: messageButtonOrigin, size: buttonSize)
		sendMessageButton.layer.cornerRadius = buttonSize.height / 3
		
		let friendsButtonX = messageButtonX + buttonSizeLength + 2 * insets
		let friendsButtonY = friendsImage.frame.origin.y + 100 + 2 * insets
		let friendsButtonOrigin = CGPoint(x: friendsButtonX, y: friendsButtonY)
		becomeFriendsButton.frame = CGRect(origin: friendsButtonOrigin, size: buttonSize)
		becomeFriendsButton.layer.cornerRadius = buttonSize.height / 3
	}
	
	private func setPhotosButtonFrame() {
		let buttonSize = CGSize(width: bounds.width, height: 30)
		let buttonX: CGFloat = 0
		let buttonY = sendMessageButton.frame.origin.y + sendMessageButton.frame.size.height + 2 * insets
		let buttonOrigin = CGPoint(x: buttonX, y: buttonY)
		showPhotosButton.frame = CGRect(origin: buttonOrigin, size: buttonSize)
	}

	// MARK: - Actions
	@IBAction func openInfoPage(_ sender: UIButton) {
	}
	
	@IBAction func becomeFriends(_ sender: UIButton) {
		guard let id = friendId else { return }
		if becomeFriendsButton.title(for: .normal) == "У Вас в друзьях" {
			FriendsQueryServices.shared.deleteFriend(with: id)
		}
		// Прописать добавление человека в друзья
	}
}
