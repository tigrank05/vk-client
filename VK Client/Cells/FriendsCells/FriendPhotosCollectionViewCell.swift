//
//  FriendPhotosCollectionViewCell.swift
//  VK Client
//
//  Created by Тигран on 17.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class FriendPhotosCollectionViewCell: UICollectionViewCell {
    
	@IBOutlet weak var photo: UIImageView!
	
}
