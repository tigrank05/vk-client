//
//  FriendsTableCell.swift
//  VK Client
//
//  Created by Тигран on 21.02.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import AlamofireImage

final class FriendsTableCell: UITableViewCell {
	
	// MARK: - Variables
	private let insets: CGFloat = 8.0
	
    // MARK: - Outlets
	@IBOutlet weak private var friendFirstName: UILabel!
	@IBOutlet weak private var friendLastName: UILabel!
	@IBOutlet weak private var friendImage: UIImageView!
	@IBOutlet weak private var onlineLabel: UIImageView!
	
    // MARK: - Functions
	override func awakeFromNib() {
		super.awakeFromNib()
		[friendFirstName, friendLastName, friendImage, onlineLabel].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
		[friendFirstName, friendLastName, friendImage, onlineLabel].forEach { $0.backgroundColor = .white }
		onlineLabel.image = UIImage(named: "Rectangle")
		onlineLabel.isHidden = true
		onlineLabel.backgroundColor = .white
		setFirstNameFrame()
		setLastNameFrame()
		setImageFrame()
		setOnlineLabelFrame()
	}
	
	func setName(_ firstName: String, _ lastName: String) {
		friendFirstName.text = firstName
		setFirstNameFrame()
		friendLastName.text = lastName
		setLastNameFrame()
	}
	
	func setImage(_ image: UIImage?) {
		friendImage.image = image
		setImageFrame()
	}
	
	func setOnlineStatus(_ status: Bool) {
		status ? (onlineLabel.isHidden = false) : (onlineLabel.isHidden = true)
		setOnlineLabelFrame()
	}
}

	// MARK: - Frames Calculations
extension FriendsTableCell {
	private func getLabelSize(text: String, font: UIFont) -> CGSize {
		let maxWidth = (bounds.width - 9 * insets - 50) / 2
		let textBlock = CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude)
		let rect = text.boundingRect(with: textBlock, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
		let width = Double(rect.size.width)
		let height = Double(rect.size.height)
		let size = CGSize(width: ceil(width), height: ceil(height))
		return size
	}
	
	private func setFirstNameFrame() {
		let labelSize = getLabelSize(text: friendFirstName.text!, font: friendFirstName.font)
		let labelX = 4 * insets + 50
		let labelY = bounds.midY - labelSize.height / 2
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		friendFirstName.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setLastNameFrame() {
		let labelSize = getLabelSize(text: friendLastName.text!, font: friendLastName.font)
		let labelX = 5 * insets + 50 + friendFirstName.frame.width
		let labelY = bounds.midY - labelSize.height / 2
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		friendLastName.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setImageFrame() {
		let imageSizeLenght: CGFloat = 50
		let imageSize = CGSize(width: imageSizeLenght, height: imageSizeLenght)
		let imageX = 2 * insets
		let imageY = bounds.midY - imageSizeLenght / 2
		let imageOrigin = CGPoint(x: imageX, y: imageY)
		friendImage.frame = CGRect(origin: imageOrigin, size: imageSize)
		friendImage.image = friendImage.image?.af_imageRoundedIntoCircle()
	}
	
	private func setOnlineLabelFrame() {
		let labelSizeLength: CGFloat = insets
		let labelSize = CGSize(width: labelSizeLength, height: labelSizeLength)
		let labelX = bounds.maxX - 4 * insets
		let labelY = bounds.midY - labelSizeLength / 2
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		onlineLabel.frame = CGRect(origin: labelOrigin, size: labelSize)
		onlineLabel.image = onlineLabel.image?.af_imageRoundedIntoCircle()
	}

}
