//
//  DialogCell.swift
//  VK Client
//
//  Created by Тигран on 23.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import AlamofireImage

final class DialogCell: UITableViewCell {
	
	//MARK: - Variables
	private let insets: CGFloat = 8.0
	
	// MARK: - Outlets
	@IBOutlet weak private var friendsName: UILabel!
	@IBOutlet weak private var lastMessage: UILabel!
	@IBOutlet weak private var date: UILabel!
	@IBOutlet weak private var friendImage: UIImageView!

	// MARK: - Functions
	override func awakeFromNib() {
		super.awakeFromNib()
		[friendsName, lastMessage, date, friendImage].forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
		[friendsName, lastMessage, date, friendImage].forEach { $0.backgroundColor = .white }
		setNameFrame()
		setMessageFrame()
		setDateFrame()
		setImageFrame()
	}
	
	func setName(_ name: String) {
		friendsName.text = name
		setNameFrame()
	}
	
	func setMessage(_ message: String) {
		lastMessage.text = message
		setMessageFrame()
	}
	
	func setDate(_ strDate: String) {
		date.text = strDate
		setDateFrame()
	}
	
	func setImage(_ image: UIImage) {
		friendImage.image = image
		setImageFrame()
	}
}

	// MARK: - Frames Calculations
extension DialogCell {
	private func getLabelSize(text: String, font: UIFont) -> CGSize {
		let maxWidth = bounds.width - 5 * insets - 50
		let textBlock = CGSize(width: maxWidth, height: (54 - insets) / 2)
		let rect = text.boundingRect(with: textBlock, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
		let width = Double(rect.size.width)
		let height = Double(rect.size.height)
		let size = CGSize(width: ceil(width), height: ceil(height))
		return size
	}
	
	private func setNameFrame() {
		let labelSize = getLabelSize(text: friendsName.text!, font: friendsName.font)
		let labelX = 4 * insets + 50
		let labelY = bounds.midY - labelSize.height - insets * 0.5
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		friendsName.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setMessageFrame() {
		let labelSize = getLabelSize(text: lastMessage.text!, font: lastMessage.font)
		let labelX = 4 * insets + 50
		let labelY = bounds.midY + insets * 0.5
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		lastMessage.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setDateFrame() {
		let labelSize = getLabelSize(text: date.text!, font: date.font)
		let labelX = bounds.maxX - insets - labelSize.width
		let labelY = insets
		let labelOrigin = CGPoint(x: labelX, y: labelY)
		date.frame = CGRect(origin: labelOrigin, size: labelSize)
	}
	
	private func setImageFrame() {
		let imageSizeLenght: CGFloat = 50
		let imageSize = CGSize(width: imageSizeLenght, height: imageSizeLenght)
		let imageX = 2 * insets
		let imageY = bounds.midY - imageSizeLenght / 2
		let imageOrigin = CGPoint(x: imageX, y: imageY)
		friendImage.frame = CGRect(origin: imageOrigin, size: imageSize)
		friendImage.image = friendImage.image?.af_imageRoundedIntoCircle()
	}
}
