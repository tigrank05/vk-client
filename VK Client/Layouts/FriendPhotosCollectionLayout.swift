//
//  FriendPhotosCollectionLayout.swift
//  VK Client
//
//  Created by Тигран on 24.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

protocol FriendPhotosCollectionLayoutDelegate: class {
	func collectionView(_ collectionView: UICollectionView, aspectRatioForItemAtIndexPath indexPath: IndexPath) -> CGFloat
}

class FriendPhotosCollectionLayout: BaseCollectionViewLayout {
	
	weak var delegate: FriendPhotosCollectionLayoutDelegate?
	
	private var itemSize: CGSize!
	private var columnXOffset: [CGFloat]!
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		self.totalColumns = 2
	}
	
	override func calculateItemSize(for indexPath: IndexPath) {
		let contentWidthWithoutIndents = collectionView!.bounds.width - contentInsets.left - contentInsets.right
		let itemWidth = (contentWidthWithoutIndents - (CGFloat(totalColumns) - 1) * interItemsSpacing) / CGFloat(totalColumns)
		let aspectRatio = delegate!.collectionView(collectionView!, aspectRatioForItemAtIndexPath: indexPath)
		let itemHeight = itemWidth * aspectRatio
		
		itemSize = CGSize(width: itemWidth, height: itemHeight)
		
		columnXOffset = []
		
		for columnIndex in 0...(totalColumns - 1) {
			columnXOffset.append(CGFloat(columnIndex) * (itemSize.width + interItemsSpacing))
		}
	}
	
	override func columnIndexForItemAt(indexPath: IndexPath) -> Int {
		return indexPath.item % totalColumns
	}
	
	override func calculateItemFrame(indexPath: IndexPath, columnIndex: Int, columnYOffset: CGFloat) -> CGRect {
		return CGRect(x: columnXOffset[columnIndex], y: columnYOffset, width: itemSize.width, height: itemSize.height)
	}
}
