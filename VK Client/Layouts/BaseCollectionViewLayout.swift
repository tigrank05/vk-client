//
//  BaseCollectionViewLayout.swift
//  VK Client
//
//  Created by Тигран on 24.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class BaseCollectionViewLayout: UICollectionViewLayout {

	private var cachedAttributes = [IndexPath : UICollectionViewLayoutAttributes]()
	private var columnsYOffset: [CGFloat]!
	private var contentSize: CGSize?
	
	private(set) var totalItemsInSection = 0
	
	var totalColumns = 0
	var interItemsSpacing: CGFloat = 2
	
	var contentInsets: UIEdgeInsets {
		return collectionView!.contentInset
	}
	
	override var collectionViewContentSize: CGSize {
		guard let contentSize = contentSize else {
			return CGSize(width: 0, height: 0)
		}
		return contentSize
	}
	
	override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		var layoutAttributesArray = [UICollectionViewLayoutAttributes]()
		
		for (_, layoutAttribute) in cachedAttributes {
			if rect.intersects(layoutAttribute.frame) {
				layoutAttributesArray.append(layoutAttribute)
			}
		}
		return layoutAttributesArray
	}
	
	override func prepare() {
		
		cachedAttributes.removeAll()
		columnsYOffset = Array(repeating: 0, count: totalColumns)
		
		totalItemsInSection = collectionView!.numberOfItems(inSection: 0)
		
		if totalItemsInSection > 0 && totalColumns > 0 {
			
			var itemIndex = 0
			var contentSizeHeight: CGFloat = 0
			
			while itemIndex < totalItemsInSection {
				let indexPath = IndexPath(item: itemIndex, section: 0)
				self.calculateItemSize(for: indexPath)
				let columnIndex = self.columnIndexForItemAt(indexPath: indexPath)
				
				let attributeRect = calculateItemFrame(indexPath: indexPath, columnIndex: columnIndex, columnYOffset: columnsYOffset[columnIndex])
				let targetLayoutAttributes = UICollectionViewLayoutAttributes.init(forCellWith: indexPath)
				targetLayoutAttributes.frame = attributeRect
				
				contentSizeHeight = max(attributeRect.maxY, contentSizeHeight)
				columnsYOffset[columnIndex] = attributeRect.maxY + interItemsSpacing
				cachedAttributes[indexPath] = targetLayoutAttributes
				
				itemIndex += 1
			}
			
			contentSize = CGSize(width: collectionView!.bounds.width - contentInsets.left - contentInsets.right, height: contentSizeHeight)
		}
	}
	
	func columnIndexForItemAt(indexPath: IndexPath) -> Int {
		return indexPath.item % totalColumns
	}
	
	func calculateItemFrame(indexPath: IndexPath, columnIndex: Int, columnYOffset: CGFloat) -> CGRect {
		return CGRect.zero
	}
	
	func calculateItemSize(for indexPath: IndexPath) {}
}
