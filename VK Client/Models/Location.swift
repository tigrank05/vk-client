//
//  Location.swift
//  VK Client
//
//  Created by Тигран on 14.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

class Location {
	var id: Int = 0
	var title: String = ""
	var latitude: Double?
	var longitude: Double?
	var icon: String = "https://vk.com/images/places/place.png"
	var address: String = ""
	var distance: Int = 0
	
	convenience init(_ json: JSON) {
		self.init()
		id = json["id"].intValue
		title = json["title"].stringValue
		latitude = json["latitude"].doubleValue
		longitude = json["longitude"].doubleValue
		icon = json["icon"].stringValue
		address = json["address"].stringValue
		distance = json["distance"].intValue
	}
	
	convenience init(title: String, latitude: Double, longtitude: Double, address: String) {
		self.init()
		self.title = title
		self.latitude = latitude
		self.longitude = longtitude
		self.address = address
	}
	
	func getAddress() -> String {
		return "\(distance) м, \(address)"
	}
}

class LocationModel {
	private var locationsArray = [Location]()
	
	func clearAll() {
		locationsArray.removeAll()
	}
	
	func addLocations(_ locations: [Location]) {
		locationsArray.append(contentsOf: locations)
	}
	
	func count() -> Int {
		return locationsArray.count
	}
	
	func setCurrentLocation(with location: Location) {
		locationsArray.insert(location, at: 0)
	}
	
	func getLocation(for indexPath: IndexPath) -> Location {
		return locationsArray[indexPath.row]
	}
	
	func clearAllButCurrent() {
		guard locationsArray.count > 1 else { return }
		let numberOfRemovingItems = locationsArray.count - 2
		locationsArray.removeLast(numberOfRemovingItems)
	}
}
