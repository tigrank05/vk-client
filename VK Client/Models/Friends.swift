//
//  Friends.swift
//  VK Client
//
//  Created by Тигран on 04.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import RealmSwift
import SwiftyJSON

class Friend: Object {

	@objc dynamic var id: Int = 0
    @objc dynamic var firstName: String = ""
    @objc dynamic var lastName: String = ""
    @objc dynamic var smallImage: String = ""
    @objc dynamic var bigImage: String = ""
	@objc dynamic var online: Bool = false
    @objc dynamic var deactivated: String = ""
	@objc dynamic var lastSeen: Double = 0.0
	@objc dynamic var city: String = ""
	@objc dynamic var bDate: String = ""
	@objc dynamic var followers: Int = 0
	@objc dynamic var common: Int = 0
	@objc dynamic var isFriend: Bool = true
	
	var friends: [Friend] = []
	var groups: [Group] = []
	var photos: [Photo] = []
	var videosCount: Int = 0
	
	var toAnyObject: Any {
		return [
			"id": id,
			"firstName": firstName,
			"lastName": lastName,
			"smallImage": smallImage,
			"bigImage": bigImage,
			"online": online,
			"deactivated": deactivated,
			"lastSeen": lastSeen,
			"city": city,
			"bDate": bDate,
			"followers": followers,
			"common": common,
			"isFriend": isFriend
		]
	}
	
	override static func primaryKey() -> String? {
		return "id"
	}
	
	override static func indexedProperties() -> [String] {
		return ["lastName"]
	}
	
    convenience init(_ json: JSON) {
		self.init()
		id = json["id"].intValue
		firstName = json["first_name"].stringValue
		lastName = json["last_name"].stringValue
		smallImage = json["photo_50"].stringValue
		bigImage = json["photo_200_orig"].stringValue
		json["online"].intValue == 0 ? (online = false) : (online = true)
		deactivated = json["deactivated"].stringValue
		lastSeen = json["last_seen", "time"].doubleValue
		city = json["city", "title"].stringValue
		bDate = json["bdate"].stringValue
    }
	
	convenience init(user json: JSON) {
		self.init()
		id = json["id"].intValue
		firstName = json["first_name"].stringValue
		lastName = json["last_name"].stringValue
		smallImage = json["photo_50"].stringValue
		bigImage = json["photo_200_orig"].stringValue
		json["online"].intValue == 0 ? (online = false) : (online = true)
		lastSeen = json["last_seen", "time"].doubleValue
		city = json["city", "title"].stringValue
		bDate = json["bdate"].stringValue
		followers = json["followers_count"].intValue
		common = json["common_count"].intValue
		json["is_friend"].intValue == 0 ? (isFriend = false) : (isFriend = true)
	}
	
	func getFullName() -> String {
		return firstName + " " + lastName
	}
}

class RealmFriendsModel {
	
	private var friendsArray: Results<Friend> = {
			let realm = try! Realm()
			return realm.objects(Friend.self).sorted(byKeyPath: "lastName", ascending: true)
		}()
	
	private lazy var titlesOfSections: [String] = {
		var finalTitlesArr: [String] = []
		var characters = [String]()
		let rus = ["А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я"]
		let eng = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
		finalTitlesArr = rus + eng
		for friend in friendsArray {
			if let friendFirstCharacter = friend.lastName.first {
				characters.append(String(friendFirstCharacter))
			} else {}
		}
		var i = 0
		for character in finalTitlesArr {
			if characters.contains(character) {
				i += 1
			}
			else {
				finalTitlesArr.remove(at: i)
			}
		}
		return finalTitlesArr
	}()
	
	func getTitlesOfSections() -> [String] {
		return titlesOfSections
	}
	
    func getNumberOfSections() -> Int {
        return titlesOfSections.count
    }
    
    func getNumberOfRowsInSections(for section: Int) -> Int {
        var rows = 0
		let sectionTitle = titlesOfSections[section]
        for friend in friendsArray {
			if let friendFirstCharacter = friend.lastName.first {
                if sectionTitle == String(friendFirstCharacter) {
                    rows += 1
                } else { }
            } else { }
        }
        return rows
    }
	
	func getFriends() -> Results<Friend> {
		return friendsArray
	}
    
    func getFriends(forSection section: Int) -> [Friend] {
        var returnArray = [Friend]()
        let sectionTitle = titlesOfSections[section]
        for friend in friendsArray {
			if let friendFirstCharacter = friend.lastName.first {
                if sectionTitle == String(friendFirstCharacter) {
                    returnArray.append(friend)
                } else { }
            } else { }
        }
        return returnArray
    }
	
	func updateStatus(for friends: [Friend]) {
		do {
			let realm = try Realm()
			try realm.write {
				for friend in friendsArray {
					for updatedFriend in friends {
						if friend.id == updatedFriend.id {
							friend.online = updatedFriend.online
						}
					}
				}
			}
		} catch {
			print(error)
		}
	}
	
	func updateStatus(forFriendWith id: Int, with status: Bool, lastSeen: Int) {
		do {
			let realm = try Realm()
			try realm.write {
				for friend in friendsArray {
					if friend.id == id {
						friend.online = status
						friend.lastSeen = Double(lastSeen)
					}
				}
			}
		} catch {
			print(error)
		}
	}
}

class NativeFriendsModel {
	private var friendsArray = [Friend]()
	
	private lazy var titlesOfSections: [String] = {
		var finalTitlesArr: [String] = []
		var characters = [String]()
		let rus = ["А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я"]
		let eng = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
		finalTitlesArr = rus + eng
		for friend in friendsArray {
			if let friendFirstCharacter = friend.lastName.first {
				characters.append(String(friendFirstCharacter))
			} else {}
		}
		var i = 0
		for character in finalTitlesArr {
			if characters.contains(character) {
				i += 1
			}
			else {
				finalTitlesArr.remove(at: i)
			}
		}
		return finalTitlesArr
	}()
	
	func getNumberOfSections() -> Int {
		return titlesOfSections.count
	}
	
	func getTitlesOfSections() -> [String] {
		return titlesOfSections
	}
	
	func getNumberOfRowsInSections(for section: Int) -> Int {
		var rows = 0
		let sectionTitle = titlesOfSections[section]
		for friend in friendsArray {
			if let friendFirstCharacter = friend.lastName.first {
				if sectionTitle == String(friendFirstCharacter) {
					rows += 1
				} else {}
			} else {}
		}
		return rows
	}
	
	func getFriends() -> [Friend] {
		return friendsArray
	}
	
	func getFriends(forSection section: Int) -> [Friend] {
		var returnArray = [Friend]()
		let sectionTitle = titlesOfSections[section]
		for friend in friendsArray {
			if let friendFirstCharacter = friend.lastName.first {
				if sectionTitle == String(friendFirstCharacter) {
					returnArray.append(friend)
				} else { }
			} else { }
		}
		return returnArray
	}
	
	func getFriendsById(_ id: Int) -> Friend {
		let arr = friendsArray.filter { (friend) -> Bool in
			return friend.id == id ? true : false
		}
		return arr[0]
	}
	
	func clearAll() {
		friendsArray.removeAll()
	}
	
	func append(contentsOf friends: Results<Friend>) {
		friendsArray.append(contentsOf: friends)
	}
}
