//
//  Profile.swift
//  VK Client
//
//  Created by Тигран on 28.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

class Profile {
	var id: Int = 0
	var firstName: String = ""
	var lastName: String = ""
	var smallImage: String = "" // 50x50
	var bigImage: String = "" // 100x100
	
	convenience init(_ json: JSON) {
		self.init()
		id = json["id"].intValue
		firstName = json["first_name"].stringValue
		lastName = json["last_name"].stringValue
		smallImage = json["photo_50"].stringValue
		bigImage = json["photo_100"].stringValue
	}
	
	convenience init(id: Int, firstName: String, lastName: String) {
		self.init()
		self.id = id
		self.firstName = firstName
		self.lastName = lastName
	}
	
	func getFullName() -> String {
		return "\(firstName) \(lastName)"
	}
}

class ProfilesModel {
	var profilesArray = [Profile]()
	
	func getProfile(byId id: Int) -> Profile? {
		for profile in profilesArray {
			if profile.id == id {
				return profile
			}
		}
		return nil
	}
}
