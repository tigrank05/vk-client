//
//  Updates.swift
//  VK Client
//
//  Created by Тигран on 15.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

struct Updates {
	var update: [Int]
	
	init() {
		update = [Int]()
	}
	
	init(_ update: [Int]) {
		self.update = update
	}
	
	mutating func setValue(_ update: [Int]) {
		self.update = update
	}
	
	func performUpdates() {
		guard let code = update.first else { return }
		let unreadDialogsChanged = Notification.Name("unreadDialogsChanged")
		switch code {
		case 4:
			var unreadDialogs = UserDefaults.standard.integer(forKey: "Unread Dialogs")
			unreadDialogs += 1
			UserDefaults.standard.set(unreadDialogs, forKey: "Unread Dialogs")
			NotificationCenter.default.post(name: unreadDialogsChanged, object: nil)
		case 6:
			var unreadDialogs = UserDefaults.standard.integer(forKey: "Unread Dialogs")
			unreadDialogs -= 1
			UserDefaults.standard.set(unreadDialogs, forKey: "Unread Dialogs")
			NotificationCenter.default.post(name: unreadDialogsChanged, object: nil)
		case 8:
			let userId = update[1] * -1
			let lastSeen = update[3]
			RealmFriendsModel().updateStatus(forFriendWith: userId, with: true, lastSeen: lastSeen)
			RealmServices.loadFriends()
		case 9:
			let userId = update[1] * -1
			let lastSeen = update[3]
			RealmFriendsModel().updateStatus(forFriendWith: userId, with: true, lastSeen: lastSeen)
			RealmServices.loadFriends()
		default:
			break
		}
	}
}
