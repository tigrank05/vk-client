//
//  FriendRequests.swift
//  VK Client
//
//  Created by Тигран on 17.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import RealmSwift
import SwiftyJSON

class FriendRequests: Object {
	var id = RealmOptional<Int>()
	
	convenience init(_ json: JSON) {
		self.init()
		id = RealmOptional<Int>(json.intValue)
	}
}
