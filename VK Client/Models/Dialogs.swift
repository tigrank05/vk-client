//
//  Dialogs.swift
//  VK Client
//
//  Created by Тигран on 23.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

class Dialog {
	var message: Message = Message()
	var lastMessageId: Int = 0
	var unread: Int = 0
	
	convenience init(_ json: JSON) {
		self.init()
		message = Message(json["message"])
		json["out_read"].intValue > json["in_read"].intValue ? (lastMessageId = json["out_read"].intValue) : (lastMessageId = json["in_read"].intValue)
		unread = json["unread"].intValue
	}
}

class DialogsModel {
	private var dialogsArray = [Dialog]()
	let usersModel = ProfilesModel()
	let groupsModel = NativeGroupsModel()
	var viewController = UIViewController()
	var unreadDialogs: Int = 0
	
	static let shared = DialogsModel()
	
	private init() {}
	
	func getDialogs(completion: @escaping (Int) -> ()) {
		MessagesQueryServices.shared.getDialogs { [weak self] (dialogsResults, usersResults, groupsResults, error)  in
			guard let strongSelf = self else { return }
			strongSelf.dialogsArray.removeAll()
			strongSelf.usersModel.profilesArray.removeAll()
			strongSelf.groupsModel.groupsArray.removeAll()
			if let dialogs = dialogsResults {
				strongSelf.dialogsArray.append(contentsOf: dialogs)
			}
			if let users = usersResults {
				strongSelf.usersModel.profilesArray = users
			}
			if let groups = groupsResults {
				strongSelf.groupsModel.groupsArray = groups
			}
			if !error.isEmpty {
				print(error)
			}
			strongSelf.unreadDialogs = strongSelf.getUnreadDialogs()
			let userDefaults = UserDefaults.standard
			userDefaults.set(strongSelf.unreadDialogs, forKey: "Unread Dialogs")
			completion(strongSelf.unreadDialogs)
		}
	}
	
	func count() -> Int {
		return dialogsArray.count
	}
	
	func getMessage(for indexPath: IndexPath) -> Message {
		return dialogsArray[indexPath.row].message
	}
	
	func getUnreadMessages(for indexPath: IndexPath) -> Int {
		return dialogsArray[indexPath.row].unread
	}
	
	func getLastMessage() -> Message? {
		return dialogsArray.last?.message
	}
	
	func getLastMessageId(for indexPath: IndexPath) -> Int {
		return dialogsArray[indexPath.row].lastMessageId
	}
	
	func clearAll() {
		dialogsArray.removeAll()
	}
	
	func setDialogs(_ dialogs: [Dialog]) {
		dialogsArray.append(contentsOf: dialogs)
	}
	
	func getUnreadDialogs() -> Int {
		let unread = dialogsArray.map({ dialog -> Int in
			if dialog.unread > 0 {
				return 1
			}
			return 0
		})
		return unread.reduce(0, +)
	}
	
	func removeLast() {
		dialogsArray.removeLast()
	}
}
