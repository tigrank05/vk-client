//
//  NewsFeed.swift
//  VK Client
//
//  Created by Тигран on 27.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

class NewsFeed {
	var sourceId: Int = 0
	var date: Double = 0.0
	var postId: Int = 0
	var text: String = ""
	var comments: Int = 0
	var likes: Int = 0
	var reposts: Int = 0
	var views: Int = 0
	var attachments: [Attachment]?
	var nextFrom: String?
	
	convenience init(_ json: JSON) {
		self.init()
		sourceId = json["source_id"].intValue
		date = json["date"].doubleValue
		postId = json["post_id"].intValue
		text = json["text"].stringValue
		comments = json["comments", "count"].intValue
		likes = json["likes", "count"].intValue
		reposts = json["reposts", "count"].intValue
		views = json["views", "count"].intValue
		attachments = json["attachments"].arrayValue.map { Attachment($0) }
	}
}

class NewsFeedModel {
	var newsArray: [NewsFeed] = []
	var friendsArray: [Friend] = []
	var groupsArray: [Group] = []
	
	convenience init(_ news: [NewsFeed], _ friends: [Friend], _ groups: [Group]) {
		self.init()
		newsArray = news
		friendsArray = friends
		groupsArray = groups
	}
	
	func clearAll() {
		newsArray.removeAll()
		friendsArray.removeAll()
		groupsArray.removeAll()
	}
	
	func addItems(_ news: [NewsFeed], _ friends: [Friend], _ groups: [Group]) {
		newsArray.append(contentsOf: news)
		friendsArray.append(contentsOf: friends)
		groupsArray.append(contentsOf: groups)
	}
	
	func getFriend(with id: Int) -> Friend {
		return friendsArray.filter { $0.id == id }[0]
	}
	
	func getGroup(with id: Int) -> Group {
		return groupsArray.filter { $0.id == id }[0]
	}
}
