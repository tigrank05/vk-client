//
//  LongPollAnswer.swift
//  VK Client
//
//  Created by Тигран on 15.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

struct LongPollAnswer {
	var key: String = ""
	var server: String = ""
	var ts: Int = 0
	var pts: Int = 0
	
	init(_ json: JSON) {
		key = json["key"].stringValue
		server = json["server"].stringValue
		ts = json["ts"].intValue
		pts = json["pts"].intValue
	}
}
