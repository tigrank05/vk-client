//
//  Messages.swift
//  VK Client
//
//  Created by Тигран on 23.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

class Message {
	
	var id: Int = 0
	var body: String = ""
	var userId: Int = 0
	var senderId: Int = 0
	var date: Double = 0
	var isReaded: Bool = false
	var out: Int = 0
	var photo: Photo?
	var image: UIImage?
	
	convenience init(_ json: JSON) {
		self.init()
		id = json["id"].intValue
		body = json["body"].stringValue
		userId = json["user_id"].intValue
		senderId = json["from_id"].intValue
		date = json["date"].doubleValue
		json["read_state"].intValue == 0 ? (isReaded = false) : (isReaded = true)
		out = json["out"].intValue
		photo = Photo(json["attachments", 0])
	}
}
