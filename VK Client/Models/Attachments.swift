//
//  PhotoAttachment.swift
//  VK Client
//
//  Created by Тигран on 10.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import SwiftyJSON

class Attachment {
	var type: String
	var photo: Photo?
	var video: Video?
	
	init(_ json: JSON) {
		type = json["type"].stringValue
		switch type {
		case "photo":
			photo = Photo(json)
		case "video":
			video = Video(json)
		default:
			break
		}
	}
}

class Photo {
	var id: Int
	var ownerId: Int
	var smallImage: String
	var bigImage: String
	var largeImage: String
	var height: Double
	var width: Double
	
	init(_ json: JSON) {
		id = json["photo", "id"].intValue
		ownerId = json["photo", "owner_id"].intValue
		smallImage = json["photo", "photo_130"].stringValue
		bigImage = json["photo", "photo_604"].stringValue
		largeImage = json["photo_1280"].stringValue
		height = json["photo", "height"].doubleValue
		width = json["photo", "width"].doubleValue
	}
	
	init(personal json:JSON) {
		id = json["id"].intValue
		ownerId = json["owner_id"].intValue
		smallImage = json["photo_130"].stringValue
		bigImage = json["photo_604"].stringValue
		largeImage = json["photo_1280"].stringValue
		height = json["height"].doubleValue
		width = json["width"].doubleValue
	}
}

class Video {
	var id: Int
	var ownerId: Int
	var title: String
	var description: String
	var bigImage: String
	var height: Double
	var width: Double
	
	init(_ json: JSON) {
		id = json["video", "id"].intValue
		ownerId = json["video", "owner_id"].intValue
		title = json["video", "title"].stringValue
		description = json["video", "description"].stringValue
		bigImage = json["video", "photo_800"].stringValue
		height = json["video", "height"].doubleValue
		width = json["video", "width"].doubleValue
	}
}
