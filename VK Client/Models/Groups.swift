//
//  Groups.swift
//  VK Client
//
//  Created by Тигран on 11.03.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import RealmSwift
import SwiftyJSON

class Group: Object {

	@objc dynamic var id: Int = 0
	@objc dynamic var name: String = ""
	@objc dynamic var type: String = ""
	@objc dynamic var isMember: Bool = false
	@objc dynamic var membersCount: Int = 0
	@objc dynamic var smallImage: String = ""
	@objc dynamic var medImage: String = ""
	@objc dynamic var bigImage: String = ""
	
	var toAnyObject: Any {
		return [
			"id": id,
			"name": name,
			"type": type,
			"isMember": isMember,
			"membersCount": membersCount,
			"smallImage": smallImage,
			"medImage": medImage,
			"bigImage": bigImage
		]
	}
	
	override static func primaryKey() -> String? {
		return "id"
	}
	
	convenience init(_ json: JSON) {
		self.init()
		id = json["id"].intValue
		name = json["name"].stringValue
		type = json["type"].stringValue
		json["is_member"].intValue == 0 ? (isMember = false) : (isMember = true)
		membersCount = json["members_count"].intValue
		smallImage = json["photo_50"].stringValue
		medImage = json["photo_100"].stringValue
		bigImage = json["photo_200"].stringValue
	}
}

class RealmGroupsModel {
	var groupsArray: Results<Group> = {
		let realm = try! Realm()
		return realm.objects(Group.self).sorted(byKeyPath: "membersCount", ascending: false)
	}()
	
	func numberOfItems() -> Int {
		return groupsArray.count
	}
	
	func getGroups() ->  Results<Group> {
		return groupsArray
	}
	
	func deleteGroup(forIndexPath indexPath: IndexPath) {
		let groupId = groupsArray[indexPath.row].id
		let session = URLSession(configuration: .default)
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/groups.leave"
		urlComponents.queryItems = [
			URLQueryItem(name: "group_id", value: "\(groupId)"),
			URLQueryItem(name: "access_token", value: NewsFeedQueryServices.shared.token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		var urlRequest = URLRequest(url: url)
		urlRequest.httpMethod = "POST"
		let task = session.dataTask(with: urlRequest)
		task.resume()
		do {
			let realm = try Realm()
			try realm.write {
				realm.delete(groupsArray[indexPath.row])
			}
		} catch {
			print(error.localizedDescription)
		}
	}
}

class NativeGroupsModel {
	var groupsArray = [Group]()
	
	func getGroups(_ indexPath: IndexPath) -> [Group] {
		var groups = [Group]()
		if indexPath.section == 0 {
			groups = groupsArray.filter({ $0.isMember == true })
		} else if indexPath.section == 1 {
			groups = groupsArray.filter({ $0.isMember == false })
		}
		return groups
	}
	
	func getGroup(byId id: Int) -> Group {
		return (groupsArray.filter { $0.id == (id * -1) })[0]
	}
	
	func numberOfMyGroups() -> Int {
		return groupsArray.filter({ $0.isMember == true }).count
	}
	
	func numberOfNotMyGroups() -> Int {
		return groupsArray.filter({ $0.isMember == false }).count
	}
	
	func addGroup(forIndexPath indexPath: IndexPath) {
		let groupId = groupsArray[indexPath.row].id
		let session = URLSession(configuration: .default)
		var urlComponents = URLComponents()
		urlComponents.scheme = "https"
		urlComponents.host = "api.vk.com"
		urlComponents.path = "/method/groups.join"
		urlComponents.queryItems = [
			URLQueryItem(name: "group_id", value: "\(groupId)"),
			URLQueryItem(name: "access_token", value: NewsFeedQueryServices.shared.token),
			URLQueryItem(name: "v", value: "5.68")]
		
		guard let url = urlComponents.url else { return }
		var urlRequest = URLRequest(url: url)
		urlRequest.httpMethod = "POST"
		let task = session.dataTask(with: urlRequest)
		task.resume()
	}
}
