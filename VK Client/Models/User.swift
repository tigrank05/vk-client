//
//  User.swift
//  VK Client
//
//  Created by Тигран on 13.04.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import Foundation

final class User {
	
	static let current = User()
	
	private init() {}
	
	var id: String = ""
	var firstName: String = ""
	var lastName: String = ""
	var groups: [Group] = []
	var friends: [Friend] = []
	
	var toAnyObject: Any {
		return [
			"id": id,
			"firstName": firstName,
			"lastName": lastName,
			"groups": groups.map { $0.toAnyObject },
			"friends": friends.map { $0.toAnyObject }
		]
	}
}
