//
//  ProgressView.swift
//  VK Client
//
//  Created by Тигран on 17.05.2018.
//  Copyright © 2018 tigrank. All rights reserved.
//

import UIKit

class ProgressView: UIView {
	
	private var progressLabel: UILabel = UILabel()

	func createLabel() {
		progressLabel = UILabel()
		progressLabel.textColor = .white
		progressLabel.textAlignment = .center
		progressLabel.text = "0 %"
		progressLabel.font = UIFont(name: "HelveticaNeue-UltraLight", size: 40.0)
		progressLabel.translatesAutoresizingMaskIntoConstraints = false
		addSubview(progressLabel)
		addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: progressLabel, attribute: .centerX, multiplier: 1.0, constant: 0.0))
		addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: progressLabel, attribute: .centerY, multiplier: 1.0, constant: 0.0))
	}
	
	func updateProgressViewLabelWithProgress(_ percent: Float) {
		progressLabel.text = String(format: "%.0f %@", percent, "%")
	}
	
	func didFinishWithError() {
		progressLabel.text = "Ошибка"
	}
}
